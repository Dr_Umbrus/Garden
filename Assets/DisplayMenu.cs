﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisplayMenu : MonoBehaviour
{
    public GameObject MenuPrincipal;
    public GameObject MenuQuitter;
    public GameObject MenuJouer;

    public void displayPrincipal()
    {
        MenuPrincipal.SetActive(!MenuPrincipal.activeSelf);
    }

    public void displayQuitter()
    {
        MenuQuitter.SetActive(!MenuQuitter.activeSelf);
    }

    public void displayJouer()
    {
        MenuJouer.SetActive(!MenuJouer.activeSelf);
    }
}
