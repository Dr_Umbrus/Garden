﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public class Telephone : MonoBehaviour
{
    
    public enum PhoneStates { Sleep, MainMenu, Storage, Options };
    PickPhone pick;

    public GameManager gm;
    ObjectListState ols;
    ScriptableOptions so;
    RangementTel rt;

    public GraphicRaycaster gr;

    [Header("Telephone")]
    public PhoneStates state = PhoneStates.Sleep;
    PhoneStates lastState = PhoneStates.MainMenu;
    public TMP_Text[] heures;

    [Header("UI States")]
    public GameObject mainMen;
    public GameObject storMen, opMun;

    [Header("Batches")]
    public GameObject pasNouveauBatch;
    public GameObject pingNouveauBatch;
    public GameObject notifNouveauBatch;
    bool isNouveauBatch=false;
    public float clignotementTime;
    public float currentTime = 0;
    bool clignotementOn=false;
    public Button[] boutons;


    [Header("Options")]
    public Slider brightness;
    public Slider master, music, sound;
    int oldRes = 0;
    public TMP_Text resoText;

    [Header("Rangement")]
    public Image[] itemsOnScreen;
    public Image[] contours;
    public TMP_Text currentNumberOfItems;
    public TMP_Text currentBatchText;
    public int currentBatch=0;
    public int maxBatch=0;
    public int[] id;
    public int currentlySelected = -1;
    int numberOfDeskItems=0;
    public bool[] toDesk;
    public bool[] toStorage;
    public Image logoObjet;

    [Header("Description")]
    public TMP_Text description;
    public Image photoGrande;

    [Header("Vibrations")]
    public float maxVibrationTime;
    float vibrationTime;
    public float maxVibrationStrength;
    float vibrationStrength;
    public float dampening;
    bool vibrating = false;
    [FMODUnity.EventRef]
    public string vibringSong, newBatchSong, noNewBatch;
    public FMOD.Studio.EventInstance soundevent;
    Vector3 startPosition = Vector3.zero;

    bool vibingCat = true;

    private void Awake()
    {
        pick = GetComponent<PickPhone>();
        rt = GetComponent<RangementTel>();
        gm = FindObjectOfType<GameManager>();
        so = gm.so;
        ols = gm.ols;
        toDesk = new bool[ols.shelfed.Length];
        toStorage = new bool[ols.shelfed.Length];
        id = new int[itemsOnScreen.Length];
        soundevent = FMODUnity.RuntimeManager.CreateInstance(vibringSong); 
    }

    private void Update()
    {
        for (int i = 0; i < heures.Length; i++)
        {
            if (System.DateTime.Now.Minute < 10)
            {
                heures[i].text = System.DateTime.Now.Hour.ToString() + ":0" + System.DateTime.Now.Minute.ToString();
            }
            else
            {
                heures[i].text = System.DateTime.Now.Hour.ToString() + ":" + System.DateTime.Now.Minute.ToString();
            }
            
        }

        FMODUnity.RuntimeManager.AttachInstanceToGameObject(soundevent, GetComponent<Transform>(), GetComponent<Rigidbody>());
        if (state == PhoneStates.Storage)
        {
            MonitorMouse();
        }

        if(state!= PhoneStates.Sleep)
        {
            CheckMouseOut();
        }

        if(state==PhoneStates.Sleep && isNouveauBatch)
        {
            if (clignotementOn)
            {
                currentTime += Time.deltaTime;
                if (currentTime >= clignotementTime)
                {
                    clignotementOn = false;
                    gm.uiS.LightDownPhone();
                }
            }
            else
            {
                currentTime -= Time.deltaTime*2;
                if (currentTime <= 0)
                {
                    clignotementOn = true;
                    gm.uiS.LightUpPhone();
                    notifNouveauBatch.SetActive(true);
                    startPosition = transform.position;
                    vibrating = true;
                    vibrationStrength = maxVibrationStrength;
                    vibrationTime = maxVibrationTime;
                }
            }

            if (vibrating)
            {
                Vibration();
            }

            
        }
    }

    void CheckMouseOut()
    {
        if (Input.GetMouseButtonDown(0))
        {
            PointerEventData pointerData = new PointerEventData(EventSystem.current);
            List<RaycastResult> results = new List<RaycastResult>();

            //Raycast using the Graphics Raycaster and mouse click position
            pointerData.position = Input.mousePosition;
            gr.Raycast(pointerData, results);

            if (results.Count > 0)
            {
                bool ok = false;
                for (int i = 0; i < results.Count; i++)
                {
                    if (results[i].gameObject.tag == "phone")
                    {
                        ok = true;
                    }
                }
                if (ok == false)
                {
                    ResetUberBeforeSleep();
                }
            }
            else
            {
                ResetUberBeforeSleep();
            }
        }
    }

    #region states
    void ResetUberBeforeSleep()
    {
        gm.countObjects();
        if(gm.currentItems <= 5)
        {
            for (int i = 0; i < toDesk.Length; i++)
            {
                toDesk[i] = false;
                toStorage[i] = false;
            }
            pick.TakeLeave();
        }
    }

    public void ButtonState(int newState)
    {
            switch (newState)
            {
                case 0:
                    ChangeState(PhoneStates.Sleep);
                    break;
                case 1:
                    ChangeState(PhoneStates.MainMenu);
                    gm.countObjects();
                    break;
                case 2:
                    ChangeState(PhoneStates.Options);
                    break;
                case 3:
                    ChangeState(PhoneStates.Storage);
                    break;
            }
        
    }

    public void ChangeState(PhoneStates newState)
    {        
        switch (newState)
        {
            case PhoneStates.Sleep:
                lastState = state;
                mainMen.SetActive(false);
                storMen.SetActive(false);
                opMun.SetActive(false);
                gm.uiS.LightDownPhone();
                if (isNouveauBatch)
                {
                    currentTime = clignotementTime;
                    clignotementOn = false;
                }
                gm.Save();
                break;
            case PhoneStates.MainMenu:
                mainMen.SetActive(true);
                storMen.SetActive(false);
                opMun.SetActive(false);

                break;
            case PhoneStates.Storage:
                mainMen.SetActive(false);
                storMen.SetActive(true);
                opMun.SetActive(false);


                numberOfDeskItems = 0;
                for (int i = 0; i < ols.shelfed.Length; i++)
                {
                    toStorage[i] = false;
                    if (!ols.shelfed[i] && ols.unlocked[i])
                    {
                        numberOfDeskItems++;
                        toDesk[i] = true;
                    }
                    else
                    {
                        toDesk[i] = false;        
                    }
                }
                UpdateView();

                break;
            case PhoneStates.Options:
                oldRes = so.currentResolution;
                if (so.resolutions.Length > 0)
                {
                    resoText.text = so.resolutions[so.currentResolution].width + "x" + so.resolutions[so.currentResolution].height;
                }
                else
                {
                    resoText.text = "1920x1080";
                }
                mainMen.SetActive(false);
                storMen.SetActive(false);
                opMun.SetActive(true);
                brightness.value = so.brightness;
                master.value = so.masterLevel;
                music.value = so.musiclevel;
                sound.value = so.soundLevel;
                break;
        }

        state = newState;
        if (state != PhoneStates.Sleep)
        {
            gm.uiS.LightUpPhone();
            notifNouveauBatch.SetActive(false);
        }
        else
        {
            gm.uiS.LightDownPhone();
        }

        if(state != PhoneStates.Options)
        {
            if (so.resolutions.Length > 0)
            {
                if (oldRes != so.currentResolution)
                {
                    gm.ChangeResolution(so.currentResolution);
                    oldRes = so.currentResolution;
                }
            }

        }
    }
    #endregion

    public void End()
    {
        gm.Quit();
    }

    public void BoutonMessage()
    {
        if (isNouveauBatch)
        {
            FMODUnity.RuntimeManager.PlayOneShotAttached(newBatchSong, gameObject);
            gm.ap.nextBatch();
            pingNouveauBatch.SetActive(false);
            isNouveauBatch = false;
            currentTime = 0;
            clignotementOn = false;
            notifNouveauBatch.SetActive(false);
            vibrating = false;
            pick.TakeLeave();
        }
        else
        {
            FMODUnity.RuntimeManager.PlayOneShotAttached(noNewBatch, gameObject);
            pasNouveauBatch.SetActive(true);
            for (int i = 0; i < boutons.Length; i++)
            {
                boutons[i].enabled = false;
            }
        }
    }

    public void AddBatchNotif()
    {
        pingNouveauBatch.SetActive(true);
        isNouveauBatch = true;
        currentTime = 0;
        clignotementOn = false;
        notifNouveauBatch.SetActive(true);
    }

    public void RemoveNotif()
    {
        pasNouveauBatch.SetActive(false);
        for (int i = 0; i < boutons.Length; i++)
        {
            boutons[i].enabled = true;
        }
    }

    #region Rangement
    public void ChangeBatch(bool right)
    {
        maxBatch = ols.currentBatch;

        if (right)
        {
            if (currentBatch == maxBatch)
            {
                currentBatch = 0;
            }
            else
            {
                currentBatch++;
            }
        }
        else
        {
            if (currentBatch == 0)
            {
                currentBatch = maxBatch;
            }
            else
            {
                currentBatch--;
            }
        }

        UpdateView();
    }

    public void UpdateView()
    {
        bool ok = false;
        int nextInBatch = 0;
        for (int i = 0; i < itemsOnScreen.Length; i++)
        {
            ok = false;
            nextInBatch = 0;
            
            for (int j = 0; j < ols.correspondingBatch.Length; j++)
            {
                if (ols.correspondingBatch[j] == currentBatch)
                {
                    if (nextInBatch == i && !ok)
                    {
                        id[i] = j;
                        ok = true;
                    }
                    else
                    {
                        nextInBatch++;
                    }
                }
            }

            if (ok)
            {
                itemsOnScreen[i].color = Color.white;
                itemsOnScreen[i].sprite = ols.photos[id[i]];

                if (toStorage[id[i]])
                {
                    contours[i].color = Color.red;
                }
                else if (toDesk[id[i]] || (currentlySelected!=i && !ols.shelfed[id[i]]))
                {
                    contours[i].color = Color.green;
                }
                else if (currentlySelected==i)
                {
                    contours[i].color = Color.grey;
                }
                else
                {
                    contours[i].color = Color.white;
                }
            }
            else
            {
                itemsOnScreen[i].color = Color.clear;
                contours[i].color = Color.clear;
            }
        }

        currentBatchText.text = (currentBatch + 1).ToString();
        currentNumberOfItems.text = numberOfDeskItems.ToString() + " / 5";

        if (numberOfDeskItems > 5)
        {
            logoObjet.color = Color.red;
        }
        else
        {
            logoObjet.color = Color.white;
        }
    }

    public void MonitorMouse()
    {
        if (Input.GetMouseButtonDown(1))
        {
            PointerEventData pointerData = new PointerEventData(EventSystem.current);
            List<RaycastResult> results = new List<RaycastResult>();

            //Raycast using the Graphics Raycaster and mouse click position
            pointerData.position = Input.mousePosition;
            gr.Raycast(pointerData, results);



            if (results.Count > 0)
            {
                
                GameObject target = results[0].gameObject;
                for (int i = 0; i < itemsOnScreen.Length; i++)
                {
                    if (target == itemsOnScreen[i].gameObject && itemsOnScreen[i].color != Color.clear)
                    {
                        int counting = 0;
                        int finalCount = 0;
                        for (int j = 0; j < ols.appDescriptions.Length; j++)
                        {
                            if (ols.correspondingBatch[j] == currentBatch)
                            {
                                if (counting == i)
                                {
                                    finalCount = j;
                                }
                                counting++;
                            }
                        }
                        ChangeDescription(finalCount);
                        currentlySelected = i;
                        UpdateView();
                        break;
                    }
                }
            }
        }

        else if (Input.GetMouseButtonDown(0))
        {
            PointerEventData pointerData = new PointerEventData(EventSystem.current);
            List<RaycastResult> results = new List<RaycastResult>();

            //Raycast using the Graphics Raycaster and mouse click position
            pointerData.position = Input.mousePosition;
            gr.Raycast(pointerData, results);


            
            if (results.Count > 0)
            {
                GameObject target = results[0].gameObject;

                    for (int i = 0; i < itemsOnScreen.Length; i++)
                    {


                        if (target == itemsOnScreen[i].gameObject && itemsOnScreen[i].color != Color.clear)
                        {
                            if (!toDesk[id[i]])
                            {
                                if (numberOfDeskItems < 5)
                                {
                                    toDesk[id[i]] = true;
                                    toStorage[id[i]] = false;
                                    numberOfDeskItems++;
                                }
                                
                            }
                            else if (ols.shelfed[id[i]])
                            {
                                toDesk[id[i]] = false;
                                toStorage[id[i]] = false;
                                numberOfDeskItems--;
                            }
                            else
                            {
                                toDesk[id[i]] = false;
                                toStorage[id[i]] = true;
                                numberOfDeskItems--;
                            }

                            UpdateView();
                            break;
                        }
                    }
                
            }
        }
    }

    public void ChangeDescription(int targetDes)
    {
        if (targetDes == -1)
        {
            photoGrande.color = Color.clear;
            description.text = "";
        }
        else
        {
            photoGrande.color = Color.white;
            photoGrande.sprite = ols.photos[targetDes];
            description.text = ols.appDescriptions[targetDes];
        }
    }

    public void SendObjects()
    {
        List<int> itemsToSend = new List<int>();
        int[] itemsToGet = new int[5];

        for (int i = 0; i < 5; i++)
        {
            itemsToGet[i] = -1;
        }

        for (int i = 0; i < toDesk.Length; i++)
        {
            if (toDesk[i])
            {
                for (int j = 0; j < 5; j++)
                {
                    if (itemsToGet[j] == -1)
                    {
                        itemsToGet[j] = i;
                        break;
                    }
                }
            }

            else if (toStorage[i])
            {
               itemsToSend.Add(i);
            }          
        }

        rt.StoreItems(itemsToSend);
        rt.UnStoreItems(itemsToGet);

        for (int i = 0; i < toDesk.Length; i++)
        {
            toStorage[i] = false;
            toDesk[i] = false;
        }

        numberOfDeskItems = 0;
        for (int i = 0; i < ols.shelfed.Length; i++)
        {
            if(!ols.shelfed[i] && ols.unlocked[i])
            {
                numberOfDeskItems++;
                toDesk[i] = true;
            }
        }
        UpdateView();
    }

    #endregion

    #region Options
    public void ChangeLights(float newBright)
    {
        gm.ChangeLights(newBright);
    }

    public void ChangeMasterVol(float newLevel)
    {
        gm.ChangeMasterVol(newLevel);
    }

    public void ChangeMusicVol(float newLevel)
    {
        gm.ChangeMusicVol(newLevel);
    }

    public void ChangeSoundVol(float newLevel)
    {
        gm.ChangeSoundVol(newLevel);
    }

    public void ChangeResolution(bool plus)
    {
        if (so.resolutions.Length > 0)
        {
            if (plus)
            {
                if (so.currentResolution < so.resolutions.Length - 1)
                {
                    so.currentResolution++;
                }
                else
                {
                    so.currentResolution = 0;
                }
            }
            else
            {
                if (so.currentResolution > 0)
                {
                    so.currentResolution--;
                }
                else
                {
                    so.currentResolution = so.resolutions.Length - 1;
                }
            }

            resoText.text = so.resolutions[so.currentResolution].width + "x" + so.resolutions[so.currentResolution].height;

        }
        
    }

    public void ValidateResolution()
    {
        if (oldRes != so.currentResolution)
        {
            gm.ChangeResolution(so.currentResolution);
            oldRes = so.currentResolution;
        }
    }
    #endregion

    void Vibration()
    { 
        FMOD.Studio.PLAYBACK_STATE fmodPbState;
        soundevent.getPlaybackState(out fmodPbState);

        if (vibingCat == true && fmodPbState != FMOD.Studio.PLAYBACK_STATE.PLAYING)
         {
                soundevent.start();
            Debug.Log("VibringOn");
            vibingCat = false;
        }
        if (state == PhoneStates.Sleep)
        {

            Vector2 circle = Random.insideUnitCircle * vibrationStrength;
            transform.position = startPosition + new Vector3(circle.x, 0, circle.y);
        }
        vibrationStrength *= dampening;
        vibrationTime -= Time.deltaTime;

        if (vibrationTime <= 0)
        {
            if (state == PhoneStates.Sleep)
            {
                transform.position = startPosition;
            Debug.Log("VibringOff");
            soundevent.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
            }
            vibrating = false;
            vibingCat = true;
        }
    }
}
