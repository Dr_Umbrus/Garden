﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangementTel : MonoBehaviour
{
    public ObjectListState ols;
    public ArchProgress arch;
    public GameManager gm;
    public Vector3[] deskPos;
    public Vector3 storagePos;
    public Transform[] deskTransform;

    public List<Vector3> itemPosForStorage;

    public List<Transform> itemsToStore;
    public Transform[] itemsToUnStore;

    bool isMoving = false;
    float lerpTime = 0;
    public float lerpSpeed = 1;

    public Vector3[] itemPosForDesk;

    private void Awake()
    {
        gm = FindObjectOfType<GameManager>();
        for (int i = 0; i < 5; i++)
        {
            deskPos[i] = deskTransform[i].position;
        }
    }

    private void Update()
    {
        if (isMoving)
        {
            lerpTime += Time.deltaTime * lerpSpeed;
            for(int i = 0; i < itemsToStore.Count; i++)
            {
                if (itemsToStore[i] != null)
                {
                    itemsToStore[i].position = Vector3.Lerp(itemPosForStorage[i], storagePos, lerpTime);               
                }               
            }
            for (int i = 0; i < itemsToUnStore.Length; i++)
            {
                if (itemsToUnStore[i] != null)
                {
                   itemsToUnStore[i].position = Vector3.Lerp(itemPosForDesk[i], deskPos[i], lerpTime);
                }
            }

            if (lerpTime >= 1)
            {
                isMoving = false;
                for (int i = 0; i < deskPos.Length; i++)
                {
                    Debug.Log(i);
                    itemsToUnStore[i] = null;
                    itemPosForDesk[i] = Vector3.zero;
                }
                FindObjectOfType<GameManager>().Save();
            }
        }
    }

    public void StoreItems(List<int> items)
    {
        itemsToStore.Clear();
        itemPosForStorage.Clear();
        for (int i = 0; i < items.Count; i++)
        {            
            if (items[i] >= 0)
            {
                itemsToStore.Add(arch.allObjectsPos[items[i]]);
                itemPosForStorage.Add(arch.allObjectsPos[items[i]].position);
                ols.shelfed[items[i]] = true;
            }
            
        }
        gm.countObjects();
    }

    public void UnStoreItems(int[] items)
    {
        
        for (int i = 0; i < items.Length; i++)
        {
            if (items[i] >= 0)
            {
                itemsToUnStore[i] = arch.allObjectsPos[items[i]];
                itemPosForDesk[i] = arch.allObjectsPos[items[i]].position;
                ols.shelfed[items[i]] = false;
            }
            
        }

        lerpTime = 0;
        isMoving = true;
        gm.countObjects();
    }
}
