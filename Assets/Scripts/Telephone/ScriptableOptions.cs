﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[CreateAssetMenu(fileName = "Options", menuName = "ScriptableObjects/ScriptableOption", order = 6)]
public class ScriptableOptions : ScriptableObject
{
    public float masterLevel;
    public float soundLevel;
    public float musiclevel;
    public float brightness;
    public int currentResolution;
    public int currentLanguage;
    public bool transcriptOn;
    public bool autoTranslate;
    public Resolution[] resolutions;

    public ScriptableColor brightest, darkest;

    public void CheckRes()
    {
        resolutions = Screen.resolutions.Where(resolution => resolution.refreshRate == 60).ToArray();
    }

    public void LoadMe(SettingsData sd)
    {

        if (sd != null)
        {
            masterLevel = sd.masterLevel;
            soundLevel = sd.soundLevel;
            musiclevel = sd.musiclevel;
            brightness = sd.brightness;
            currentResolution = sd.resolution;
            currentLanguage = sd.currentLanguage;
            transcriptOn = sd.transcriptOn;
            autoTranslate = sd.autoTranslate;
        }
        else
        {
            masterLevel = 1;
            soundLevel = 1;
            musiclevel = 1;
            brightness = 1;
            currentResolution = resolutions.Length/2;
            currentLanguage = 0;
            transcriptOn = false;
            autoTranslate = false;
        }
    }
}
