﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Color", menuName = "ScriptableObjects/ScriptableColor", order = 1)]
public class ScriptableColor : ScriptableObject
{
    public Color col;
}
