﻿//nécessaire pour faire un éditeur
using UnityEditor;
using UnityEngine;


//Test d'editor window. 
public class Window : EditorWindow
{

    /// <summary>
    /// Cette fenêtre sert à invoquer un game object vert, et à changer la couleur des spriterenderer sur les objets sélectionnés.
    /// </summary>
    /// 

    //Un game object à mettre
    public GameObject gamo;
    //Les Scriptable Colors. (Scriptable Object avec une couleur dessus)
    public ScriptableColor[] cols;
    //Une couleur.
    Color color;

    //L'array contenant les noms des scriptable colors, pour le dropdown.
    string[] options;
    //L'index de quelle couleur on prend.
    int index=-1;
   
    //Permet d'avoir cette fenêtre dans le menu Window, sous le nom Colorizer.
    //[MenuItem("Window/Colorizer")]

    //Sert à créer la fenêtre.
    public static void ShowWindow()
    {
        GetWindow<Window>("Colorizer");
    }

    //Au moment où on l'active
    private void OnEnable()
    {
        
        //On récupère le nom de tous les ScriptableColor. "t:" + typeof(type) = récupérer les assets du type choisi.
        //new[] { "Assets/Colors" } = chercher uniquement dans le dossier Colors, dans Assets.
        //AssetDatabase.FindAssets ne renvoie que des strings, d'où l'importance de faire un array de string ici.
        string[] str= AssetDatabase.FindAssets("t:" + typeof(ScriptableColor).Name, new[] { "Assets/Colors" });

        //On initialise l'array de scriptablecolor pour qu'il ait la même taille que le nombre qu'on a trouvé avec le FindAssets. Pareil pour options.
        cols = new ScriptableColor[str.Length];
        options = new string[str.Length];


        for (int i = 0; i < str.Length; i++)
        {
            //On récupère le chemin pour accéder à la scriptablecolor.
            string path = AssetDatabase.GUIDToAssetPath(str[i]);
            //On charge ensuite la ScriptableColor associée à ce chemin dans l'array.
            cols[i]= AssetDatabase.LoadAssetAtPath<ScriptableColor>(path);
            //Et on récupère le nom du fichier pour le placer dans options. /!\ ce nom n'est pas la même chose que "t:" + typeof(ScriptableColor).Name
            options[i] = cols[i].name;
        }
    }

    //Quand on affiche la fenêtre.
    private void OnGUI()
    {
        //GUILayout.Label = créer un nom. "Objet" = nom du label. EditorStyles = les styles à appliquer (ici bold)
        GUILayout.Label("Objet", EditorStyles.boldLabel);

        /*On détermine le gameobject à spawn.
        EditorGUILayout.ObjectField = un champ dans lequel on met un objet.
        "prefab" = le nom du champ. gamo = l'objet.
        typeof = quel type d'objet (ici GameObject)
        le bool correspond à si on accepte les objets de la scène.
        Ne pas oublier le "as type" à la fin, pour que le cast se fasse correctement.*/
        gamo = EditorGUILayout.ObjectField("prefab", gamo, typeof(GameObject), true) as GameObject;

        //Le choix de couleur.
        GUILayout.Label("Color object", EditorStyles.boldLabel);
        //EditorGUILayout.ColorField = un champ dans lequel on choisit une couleur.
        color = EditorGUILayout.ColorField("ColorPot", color);

        //Le label pour la liste des scriptablecolor
        GUILayout.Label("Color list", EditorStyles.boldLabel);
        //L'int index permet de récupérer l'option sélectionnée dans le Popup. L'array options permet d'utiliser les noms des fichiers dans le popup.
        index = EditorGUILayout.Popup(index, options);

        //Si on a choisi dans la liste, la couleur correspond à celle qui se trouve dans le scriptable color.
        if (index >=0)
        {
            color = cols[index].col;
        }

        //Quand on click sur ce bouton, indiqué COLOR.
        if (GUILayout.Button("COLOR"))
        {

            //Tous les objets sélectionnés
            foreach(GameObject obj in Selection.gameObjects)
            {
                //S'il y a un SpriteRenderer dessus, on change sa couleur.
                SpriteRenderer rend = obj.GetComponent<SpriteRenderer>();
                if (rend != null)
                {
                    rend.color = color;
                }
            }

            //Et on instantie le gamo en 2,2,2 en le mettant en vert et tourné à 45°.
           GameObject pot = Instantiate(gamo, new Vector3(2, 2, 2), Quaternion.identity);
            pot.GetComponent<SpriteRenderer>().color = Color.green;
            pot.transform.Rotate(new Vector3(0, 0, 45));

        }

        if (GUILayout.Button("RANDOM"))
        {
            index = -1;
            color = Random.ColorHSV();
            //rc.GenerateColor();
        }

        //S on appuie sur le bouton RESET
        if (GUILayout.Button("RESET"))
        {
            //On met l'index à -1 (on sort de la liste des scriptablecolor donc), et on met la couleur par défaut comme du noir à opacité maximale.
            index = -1;
            color = new Color(0, 0, 0, 1);
        }
    }
}
