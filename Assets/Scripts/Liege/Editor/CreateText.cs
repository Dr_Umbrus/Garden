﻿using UnityEditor;
using UnityEngine;

//Classe EditorWindow pour que ça soit une fenêtre et pas un script d'objet
public class CreateText : EditorWindow
{
    //Le GO sur lequel on met le texte
    public GameObject baseObject = null;

    public bool sceneObject = true;

    public string parentName="parent vide";

    //Le ScriptableObject qui contient les infos sur les lettres
    public ScriptableLetterManager slm;

    //Le nombre de lettres dans le texte
    public int numberLettres = 3;

    //Une variable de comparaison pour savoir quand ajuster les tableaux
    int oldNumberLettre = 3;

    //L'array de GO qui constitue la phrase
    public GameObject[] fullText = new GameObject[3];

    //La distance entre deux lettres, en unités unity
    public float espacementLettres = 1.5f;

    //Le multiplicateur appliqué à la taille des lettres, par rapport au prefab
    public float fontSize = 1;

    //La couleur du texte, actuellement inutile
    public Color fontColor=Color.cyan;

    //La position d'apparition des lettres, par rapport à l'objet
    public Vector3 startPosition = Vector3.zero;

    //Si on prend en compte les axe X,Y,Z dans l'application de position. Si tout est en true, les lettres vont viser le centre de l'objet.
    public bool getY = true, getX = true, getZ = true;

    //Spawn en ligne ou en cercle.
    public bool line = true;

    public Vector3 additionalAngle = Vector3.zero;

    //int pour savoir comment aligner les lettres.
    public int axisInt = 0;

    public int circleArc = 360;

    public float circleRadius=10;

    //Le texte pour axisInt, permet de choisir si on aligne les lettres sur l'axe X, Y, Z.
    string[] axis = new string[6] { "X", "Y", "Z", "XY", "XZ", "YZ"};

    //Un int pour gérer quels GO pour chaque lettre 
    int[] currentIndex = new int[3];

    //Cette ligne sert à mettre la fenêtre dans le menu Window, sous menu OutilsPerso
    [MenuItem("Window/OutilsPerso/TextCreator")]

    //La fonction pour créer la fenêtre
    public static void ShowWindow()
    {
        GetWindow<CreateText>("TextCreator");
    }

    //Au moment où on l'active
    private void OnEnable()
    {
        //On récupère tous les assets dans le dossier Lettres, qui sont des ScriptableLetterManager. Il ne doit y en avoir qu'un seul.
        string[] str = AssetDatabase.FindAssets("t:" + typeof(ScriptableLetterManager).Name, new[] { "Assets/Lettres" });
        //On récupère le chemin vers cet asset
        string path = AssetDatabase.GUIDToAssetPath(str[0]);
        //Et on attribue cet asset à la variable slm
        slm = AssetDatabase.LoadAssetAtPath<ScriptableLetterManager>(path);
    }

    //Ce qui s'affiche. Un peu comme "update", en soi
    private void OnGUI()
    {
        //Attribuer l'objet sur lequel on met le texte. EditorGUILayout.ObjectField = un emplacement où on sélectionne un objet.
        //Entre guillemets le nom de la ligne. Ensuite, sur quelle variable on applique ça. Dire qu'on veut un GO. Le true, c'est pour dire qu'on accepte les objets de la scène. Et on cast en GO.
        baseObject = EditorGUILayout.ObjectField("Objet à marquer", baseObject, typeof(GameObject), true) as GameObject;

        sceneObject = EditorGUILayout.Toggle("Scene ?", sceneObject);

        parentName = EditorGUILayout.TextField("nom du parent", parentName);

        //EditorGUILayout.IntField = Un champ pour mettre un nombre entier.
        numberLettres = EditorGUILayout.IntField("nombre lettres", numberLettres);

        line = EditorGUILayout.Toggle("Line ?", line);

        //Si le nombre de lettres à changé depuis la dernière frame
        if (oldNumberLettre != numberLettres)
        {
            //On update le nombre
            oldNumberLettre = numberLettres;

            //On créé un tableau temporaire qui stocke les int
            int[] temp = currentIndex;

            //On reset les deux array avec la nouvelle taille.
            fullText = new GameObject[numberLettres];
            currentIndex = new int[numberLettres];

            //On calcule si l'ancien tableau ou le nouveau est plus petit, et on stocke cette info (éviter de sortir des limites des array)
            int lower = temp.Length-1;
            if (fullText.Length < temp.Length)
            {
                lower = fullText.Length;
            }

            //Puis on remet les int dans le nouveau tableau. Si le nouveau est plus grand, tous les int en plus sont initialisés à 0. Si le nouveau est plus petit, on cut les int en trop.
            for (int i = 0; i < lower; i++)
            {
                currentIndex[i] = temp[i];
            }
        }

        //Un label, pour marquer le début de la catégorie
        GUILayout.Label("Texte", EditorStyles.boldLabel);

        //Pour chaque lettre du texte
        for (int i = 0; i < fullText.Length; i++)
        {
            //On récupère un int qui correspond à la lettre. 
            //EditorGUILayout.Popup = Une liste en dropdown. Avec un nom, sur quel variable on l'applique, et un array de strings pour le nom des options.
            currentIndex[i]= EditorGUILayout.Popup("lettre " + i, currentIndex[i], slm.nomsLettres);

            //Puis on utilise le int pour mettre dans fullText le GO qui correspond dans le slm.
            fullText[i] = slm.prefabLettres[currentIndex[i]];
        }

        if (line)
        {
            //Un champ pour mettre un nombre décimal, pour l'espace et la taille des lettres.
            espacementLettres = EditorGUILayout.FloatField("espace", espacementLettres);          
        }
        else
        {
            circleArc = EditorGUILayout.IntSlider("Arc de cercle",circleArc, 45, 360);
        }

        fontSize = EditorGUILayout.FloatField("size multiplier", fontSize);

        GUILayout.Label("Options", EditorStyles.boldLabel);

        //Un Color Picker, la couleur de textes. Ne marche pas actuellement.
        //fontColor = EditorGUILayout.ColorField("font color", fontColor);

        if (line)
        {
            //Un champ pour Vector3 pour la position de départ.
            startPosition = EditorGUILayout.Vector3Field("start pos", startPosition);
        }
        else
        {
            startPosition = EditorGUILayout.Vector3Field("repositionnement du cercle", startPosition);

            circleRadius = EditorGUILayout.FloatField("rayon du cercle", circleRadius);
        }

        additionalAngle = EditorGUILayout.Vector3Field("rotation additionnelle", additionalAngle);

        if (line)
        {
            //Un autre popup, pour choisir parmi les trois axes.
            axisInt = EditorGUILayout.Popup("axe de répartition",axisInt, axis);
        }
        else
        {
            axisInt = EditorGUILayout.Popup("axe de repositionnement",axisInt, axis);
        }
        

        //Les trois bool pour savoir quels axes sont pris en compte. Généralement gardez en au moins deux d'activés.
        getX = EditorGUILayout.Toggle("X ?", getX);
        getY = EditorGUILayout.Toggle("Y ?", getY);
        getZ = EditorGUILayout.Toggle("Z ?", getZ);

        //Quand on appuie sur le bouton marqué CREATE
        if (GUILayout.Button("CREATE"))
        {
            //Le vecteur qui servira au placement des lettres.
            Vector3 trans = Vector3.zero;

            //Selon la valeur de axisInt, le vecteur trans sera soit sur un axe soit sur une diagonale.
            if (axisInt == 0)
            {
                trans = new Vector3(1, 0, 0);
            }
            else if (axisInt == 1)
            {
                trans = new Vector3(0, 1, 0);
            }
            else if (axisInt == 2)
            {
                trans = new Vector3(0, 0, 1);
            }
            else if (axisInt == 3)
            {
                trans = new Vector3(1, 1, 0);
            }
            else if (axisInt == 4)
            {
                trans = new Vector3(1, 0, 1);
            }
            else if (axisInt == 5)
            {
                trans = new Vector3(0, 1, 1);
            }

            if (!sceneObject)
            {
                GameObject g = Instantiate(baseObject, Vector3.zero, Quaternion.identity);
                baseObject = g;
            }
            //On créé un GO vide qui sert de parent, on lui donne un nom, on le place sur l'objet principal en le mettant en enfant.
            GameObject par = new GameObject();
            par.name = parentName;
            par.transform.position = baseObject.transform.position;
            par.transform.parent = baseObject.transform;


            GameObject[] currentLetters = new GameObject[fullText.Length];

            float angle = circleArc / fullText.Length;
            //Pour chaque lettre
            for (int i = 0; i < fullText.Length; i++)
            {
                //On l'instancie simplement à la position de base : Celle de l'objet + le vecteur qu'on a défini
                GameObject temp = Instantiate(fullText[i], baseObject.transform.position, Quaternion.identity);
                temp.transform.localScale = temp.transform.localScale * fontSize;
                currentLetters[i] = temp;
                //On la met en enfant du GO vide.
                temp.transform.parent = par.transform;

                if (line)
                {
                    temp.transform.Translate(startPosition);
                    //Puis on translate pour qu'elles soient bien alignés. La formule sert à avoir "espacementLettres" comme vide entre deux objets, avec un alignement centré sur la position définie, qui suit l'axe demandé.
                    temp.transform.Translate(((-espacementLettres * fullText.Length) / 2 + (espacementLettres * i)) * trans);
                }
                else
                {
                    Quaternion rotation = Quaternion.AngleAxis(i * angle, Vector3.up);
                    Vector3 direction = rotation * Vector3.forward;

                    Vector3 position = direction * circleRadius;

                    
                    temp.transform.Translate(position);

                    Vector3 circleTrans = Vector3.zero;
                    circleTrans.x = trans.x * startPosition.x;
                    circleTrans.y = trans.y * startPosition.y;
                    circleTrans.z = trans.z * startPosition.z;

                    temp.transform.Translate(circleTrans);
                }

                


            }

            par.transform.Rotate(additionalAngle);

            for (int i = 0; i < fullText.Length; i++)
            {
                Vector3 cast = baseObject.transform.position - currentLetters[i].transform.position;

                //Si un axe est ignoré, on met sa valeur à 0.
                if (!getX)
                {
                    cast.x = 0;
                }
                if (!getY)
                {
                    cast.y = 0;
                }
                if (!getZ)
                {
                    cast.z = 0;
                }

                RaycastHit hit;

                /*On fait un simple raycast dans la direction prévue. Note perso : probablement mettre un layerMask pour être sûr que ça se met bien sur l'objet,
                mais c'est pas obligatoire si on a une scène "atelier" qui sert à marquer les objets avant de les mettre en prefab */
                if (Physics.Raycast(currentLetters[i].transform.position, cast, out hit, Mathf.Infinity))
                {
                    Debug.Log("O K ");
                    //Si on a touché l'objet, la lettre se tp sur la position où ça a touché, et on aligne sa rotation avec la normale de la surface.
                    currentLetters[i].transform.position = hit.point;
                    Debug.Log(hit.point);
                    currentLetters[i].transform.rotation = Quaternion.FromToRotation(Vector3.right, hit.normal);
                }
            }
            
        }
    }

}
