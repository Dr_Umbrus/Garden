﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "LetterMan", menuName = "ScriptableObjects/ScriptableLetterManager", order = 2)]
public class ScriptableLetterManager : ScriptableObject
{
    public GameObject[] prefabLettres;
    public string[] nomsLettres;
}
