﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptableElementTableau : ScriptableObject
{
    public bool unlocked = false;
    public bool unpin = true;
    public Sprite visual;
    public GameObject summon;
}
