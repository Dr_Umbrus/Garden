﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BobineCouleurUI : MonoBehaviour
{
    public ScriptableColor col;

    private void Awake()
    {
        GetComponent<Image>().color = col.col;
    }
}
