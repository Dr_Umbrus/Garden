﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BobineCouleur : MonoBehaviour
{
    public ScriptableColor col;

    private void Awake()
    {
        GetComponent<SpriteRenderer>().color = col.col;
    }
}
