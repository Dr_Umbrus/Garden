﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TableauLiege : MonoBehaviour
{
    public List<LineRenderer> lines;
    public List<GameObject> pined;

    public List<GameObject> lineStarts;
    public List<GameObject> lineEnds;

    public GameObject lineRend;
    public GameObject pin;

    public List<GameObject> unpined;

    bool clicIn = false;
    bool movin = false;

    public LayerMask mask;
    public Transform currentObject;

    Vector3 oldPos;

    public List<int> linesId;

    public SpriteRenderer liegeRend;

    Color currentColor;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A) && !clicIn)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            Physics.Raycast(ray, out hit, 1000);

            if (hit.collider != null)
            {
                if (hit.collider.tag == "pin")
                {
                    GameObject pinO = hit.collider.gameObject;

                    List<int> removInt = new List<int>();

                    for (int i = 0; i < lineStarts.Count; i++)
                    {
                        if (lineStarts[i] == pinO || lineEnds[i] == pinO)
                        {
                            removInt.Add(i);
                        }
                    }

                    for (int i = removInt.Count - 1; i >= 0; i--)
                    {
                        lineStarts.RemoveAt(removInt[i]);
                        Destroy(lines[removInt[i]].gameObject);
                        lines.RemoveAt(removInt[i]);
                        lineEnds.RemoveAt(removInt[i]);
                    }

                }

               
            }

            
        }

        if (Input.GetMouseButtonDown(1))
        {
            movin = !movin;

            if (movin)
            {
                liegeRend.color = Color.blue;
            }
            else
            {
                liegeRend.color = Color.white;
            }
        }

        if (Input.GetMouseButtonDown(0))
        {


            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            Physics.Raycast(ray, out hit, 1000);

            if (hit.collider != null)
            {

                if (hit.collider.tag == "pin")
                {
                    clicIn = true;

                    if (movin)
                    {
                        currentObject = hit.collider.transform;

                        if (pined.Contains(currentObject.gameObject))
                        {
                            pined.Remove(currentObject.gameObject);
                        }
                        if (unpined.Contains(currentObject.gameObject))
                        {
                            unpined.Remove(currentObject.gameObject);
                        }

                        oldPos = currentObject.position;
                        currentObject.gameObject.layer = 2;
                    }
                    else
                    {
                        GameObject temp = Instantiate(lineRend, hit.point, Quaternion.identity);
                        temp.transform.parent = transform;
                        lines.Add(temp.GetComponent<LineRenderer>());
                        //lines[lines.Count - 1].SetPosition(0, hit.point);
                        lines[lines.Count - 1].startColor = currentColor;
                        lines[lines.Count - 1].endColor = currentColor;
                        lineStarts.Add(hit.collider.gameObject);
                        lineEnds.Add(null);
                    }
                }

                else if (hit.collider.tag == "bobine")
                {
                    currentColor = hit.collider.GetComponent<BobineCouleur>().col.col;
                }
            }
        }

        if (clicIn && movin)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            Physics.Raycast(ray, out hit, 1000, mask);

            if (hit.collider != null)
            {
                currentObject.position = hit.point;
            }
        }

        /*if (clicIn)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            Physics.Raycast(ray, out hit);

            if (hit.collider != null)
            {
                lines[lines.Count - 1].SetPosition(1, hit.point);
            }
        }*/

        if (Input.GetMouseButtonUp(0) && clicIn)
        {
            if (movin)
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                Physics.Raycast(ray, out hit, mask);

                if (hit.collider != null)
                {
                    if (hit.collider.tag == "liege")
                    {
                        pined.Add(currentObject.gameObject);
                        currentObject.gameObject.layer = 0;
                        currentObject = null;
                        oldPos = Vector3.zero;
                    }
                    else if (hit.collider.tag == "pin")
                    {
                        pined.Add(currentObject.gameObject);
                        currentObject.gameObject.layer = 0;
                        currentObject.position = oldPos;
                        currentObject = null;
                        oldPos = Vector3.zero;
                    }
                    else
                    {
                        unpined.Add(currentObject.gameObject);
                        currentObject.gameObject.layer = 0;
                        currentObject = null;
                        oldPos = Vector3.zero;
                    }
                }
                else
                {
                    List<int> removInt = new List<int>();

                    for (int i = 0; i < lineStarts.Count; i++)
                    {
                        if(lineStarts[i] == currentObject.gameObject || lineEnds[i] == currentObject.gameObject)
                        {
                            removInt.Add(i);
                        }
                    }

                    for(int i=removInt.Count-1; i>=0; i--)
                    {
                        lineStarts.RemoveAt(removInt[i]);
                        Destroy(lines[removInt[i]].gameObject);
                        lines.RemoveAt(removInt[i]);
                        lineEnds.RemoveAt(removInt[i]);
                    }

                    Destroy(currentObject.gameObject);
                    currentObject = null;
                }
                clicIn = false;

            }
            else
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                Physics.Raycast(ray, out hit);

                if (hit.collider != null)
                {
                    if (hit.collider.tag == "liege")
                    {
                        GameObject temp = Instantiate(pin, hit.point, Quaternion.identity);
                        pined.Add(temp);
                        //lines[lines.Count - 1].SetPosition(1, hit.point);
                        lineEnds[lineEnds.Count - 1] = temp;
                    }
                    else if (hit.collider.tag == "pin")
                    {
                        if(hit.collider.gameObject == lineStarts[lineStarts.Count - 1])
                        {
                            Destroy(lines[lines.Count - 1].gameObject);
                            lineStarts.RemoveAt(lineStarts.Count - 1);
                            lineEnds.RemoveAt(lineEnds.Count - 1);
                            lines.RemoveAt(lines.Count - 1);
                        }
                        
                        else
                        {
                            bool ok = true;

                            for (int i = 0; i < lineStarts.Count; i++)
                            {
                                if(lineStarts[i]==lineStarts[lineStarts.Count-1] && lineEnds[i] == hit.collider.gameObject)
                                {
                                    ok = false;
                                }
                                else if (lineEnds[i] == lineStarts[lineStarts.Count - 1] && lineStarts[i] == hit.collider.gameObject)
                                {
                                    ok = false;
                                }
                            }
                            if (!ok)
                            {
                                Destroy(lines[lines.Count - 1].gameObject);
                                lineStarts.RemoveAt(lineStarts.Count - 1);
                                lineEnds.RemoveAt(lineEnds.Count - 1);
                                lines.RemoveAt(lines.Count - 1);
                            }
                            else
                            {
                                lineEnds[lineEnds.Count - 1] = hit.collider.gameObject;
                            }
                            
                        }
                        //lines[lines.Count - 1].SetPosition(1, hit.point);
                        
                    }
                }

                clicIn = false;
            }

        }

        for (int i = 0; i < lines.Count; i++)
            {
                lines[i].SetPosition(0, lineStarts[i].transform.position);
                if (lineEnds[i] == null)
                {
                    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                    RaycastHit hit;
                    Physics.Raycast(ray, out hit);

                    if (hit.collider != null)
                    {
                        lines[i].SetPosition(1, hit.point);
                    }
                    else
                    {
                        lines[i].SetPosition(1, Vector3.zero);
                    }
                }
                else
                {
                    lines[i].SetPosition(1, lineEnds[i].transform.position);
                }
            }
    }
}
