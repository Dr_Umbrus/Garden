﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class TableauLiege2 : MonoBehaviour
{
    [Header("Tableau")]
    GraphicRaycaster gr;

    public List<LineRenderer> lines;
    public List<GameObject> pined;

    public List<GameObject> lineStarts;
    public List<GameObject> lineEnds;

    public List<int> lineStartsID;
    public List<int> lineEndsID;

    public GameObject lineRend;
    public GameObject pin;

    bool clicIn = false;
    bool movin = false;

    public LayerMask mask;
    public Transform currentObject;

    Vector3 oldPos;

    public List<int> linesId;
    public List<int> linesColor;

    public Image liegeRend;


    [Header("Menu Objects")]
    public int currentPage=0;
    public int maxPage=0;
    public int objectsPerPage=3;
    bool menuOpen = false;
    public List<int> pinedId;
    public ScriptableInfoTableau infos;

    public Transform menu;
    public Vector3 minPos, maxPos;
    public float lerpPos;
    public Image[] menuImages;
    public int[] realIds;

    public ScriptableColor[] colors;
    int currentColor;
    

    private void Awake()
    {
        maxPage = Mathf.FloorToInt((infos.unlocked.Length-1) / objectsPerPage);
        gr = GetComponent<GraphicRaycaster>();
        realIds = new int[menuImages.Length];
        infos.BaseStats();
    }

    void Update()
    {
        CheckMouseRemove();
        CheckClickDown();
        ObjectManipulation();
        CheckClickUp();
        SetLinePos();
        CheckKeys();
        MenuPos();
    }

    void CheckKeys()
    {
        if (Input.GetKeyDown(KeyCode.T))
        {
            SaveSystem.SaveLiege(this);
        }

        if (Input.GetKeyDown(KeyCode.Y))
        {
            LoadSetup();
        }
    }

    void MenuPos()
    {
        if (menuOpen)
        {
            if (lerpPos < 1)
            {
                lerpPos += Time.deltaTime;
            }
            
        }
        else
        {
            if (lerpPos > 0)
            {
                lerpPos -= Time.deltaTime;
            }           
        }

        menu.localPosition = Vector3.Lerp(minPos, maxPos, lerpPos);
    }

    void MovinActivation()
    {
        movin = !movin;

        if (movin)
        {
            liegeRend.color = Color.blue;
        }
        else
        {
            liegeRend.color = Color.white;
        }
    }

    void CheckClickDown()
    {
        if (Input.GetMouseButtonDown(1) && !clicIn)
        {
            MovinActivation();
        }


        if (Input.GetMouseButtonDown(0))
        {

            PointerEventData pointerData = new PointerEventData(EventSystem.current);
            List<RaycastResult> results = new List<RaycastResult>();

            //Raycast using the Graphics Raycaster and mouse click position
            pointerData.position = Input.mousePosition;
            gr.Raycast(pointerData, results);

            if (results.Count > 0)
            {
                GameObject result = results[0].gameObject;
                Vector3 pos = results[0].worldPosition;
                if (result.tag == "pin")
                {
                    clicIn = true;

                    if (movin)
                    {
                        currentObject = result.transform;

                        oldPos = currentObject.position;
                    }
                    else
                    {
                        GameObject temp = Instantiate(lineRend, pos, Quaternion.identity);
                        temp.transform.parent = transform;
                        lines.Add(temp.GetComponent<LineRenderer>());
                        lines[lines.Count - 1].startColor = colors[currentColor].col;
                        lines[lines.Count - 1].endColor = colors[currentColor].col;
                        lineStarts.Add(result);
                        lineEnds.Add(null);
                        linesColor.Add(currentColor);
                        lineStartsID.Add(FindIDInList(result));
                        lineEndsID.Add(-1);
                    }
                }

                else if (result.tag == "menu")
                {
                    if (!movin)
                    {
                        MovinActivation();
                    }

                    int id = -1;
                    for (int i = 0; i < menuImages.Length; i++)
                    {
                        if (menuImages[i].gameObject == result)
                        {
                            id = i;
                        }
                    }

                    if (id >= 0)
                    {
                            GameObject temp = Instantiate(infos.summon[realIds[id]], Vector3.zero, Quaternion.identity, transform);
                            currentObject = temp.transform;
                            pinedId.Add(realIds[id]);
                            pined.Add(temp);
                            infos.unpinned[realIds[id]] = false;
                            oldPos = Vector3.zero;
                            RefreshVisual();
                    }

                    clicIn = true;
                }
            }



        }
    }

    void CheckClickUp()
    {
        if (Input.GetMouseButtonUp(0) && clicIn)
        {
            PointerEventData pointerData = new PointerEventData(EventSystem.current);
            List<RaycastResult> results = new List<RaycastResult>();

            //Raycast using the Graphics Raycaster and mouse click position
            pointerData.position = Input.mousePosition;
            gr.Raycast(pointerData, results);

            if (results.Count > 0)
            {
                GameObject result = results[0].gameObject;
                Vector3 pos = results[0].worldPosition;
                if (movin)
                {
                    if (result == currentObject.gameObject)
                    {
                        if (results.Count > 1)
                        {
                            result = results[1].gameObject;
                            pos = results[1].worldPosition;
                        }
                        else
                        {
                            result = gameObject;
                        }
                    }



                    if (result.tag == "liege")
                    {
                        currentObject = null;
                        oldPos = Vector3.zero;
                    }
                    else if (result.tag == "pin")
                    {
                        if (oldPos == Vector3.zero)
                        {
                            pined.RemoveAt(pined.Count - 1);
                            infos.unpinned[pinedId[pinedId.Count - 1]] = true;
                            Destroy(currentObject.gameObject);
                            RefreshVisual();
                        }
                        else
                        {
                            currentObject.position = oldPos;
                        }
                        currentObject = null;
                        oldPos = Vector3.zero;
                    }
                    else
                    {
                        if (oldPos == Vector3.zero)
                        {
                            pined.RemoveAt(pined.Count - 1);
                            infos.unpinned[pinedId[pinedId.Count - 1]] = true;
                            Destroy(currentObject.gameObject);
                            RefreshVisual();
                        }
                        else
                        {
                            int id = -1;
                            for (int i = 0; i < pined.Count; i++)
                            {
                                if (pined[i] == currentObject.gameObject)
                                {
                                    id = i;
                                }
                            }

                            if (id >= 0)
                            {
                                CheckMouseRemove();

                                pined.RemoveAt(id);

                                if (pinedId[id] > 0)
                                {
                                    
                                    infos.unpinned[pinedId[id]] = true;
                                }
                                pinedId.RemoveAt(id);

                                Destroy(currentObject.gameObject);

                                RefreshVisual();
                            }
                        }
                        currentObject = null;
                        oldPos = Vector3.zero;
                    }
                    clicIn = false;

                }
                else
                {

                    if (result.tag == "liege")
                    {
                        GameObject temp = Instantiate(pin, pos, Quaternion.identity, transform);
                        pined.Add(temp);
                        pinedId.Add(-1);
                        temp.transform.position = new Vector3(temp.transform.position.x, temp.transform.position.y, 0);
                        lineEnds[lineEnds.Count - 1] = temp;
                        lineEndsID[lineEndsID.Count - 1] = FindIDInList(temp);
                    }
                    else if (result.tag == "pin")
                    {
                        if (result == lineStarts[lineStarts.Count - 1])
                        {
                            RemoveLastLine();
                        }

                        else
                        {
                            bool ok = true;

                            for (int i = 0; i < lineStarts.Count; i++)
                            {
                                if (lineStarts[i] == lineStarts[lineStarts.Count - 1] && lineEnds[i] == result)
                                {
                                    ok = false;
                                }
                                else if (lineEnds[i] == lineStarts[lineStarts.Count - 1] && lineStarts[i] == result)
                                {
                                    ok = false;
                                }
                            }
                            if (!ok)
                            {
                                RemoveLastLine();
                            }
                            else
                            {
                                lineEnds[lineEnds.Count - 1] = result;
                                lineEndsID[lineEndsID.Count - 1] = FindIDInList(result);
                            }

                        }

                    }

                    clicIn = false;
                }
            }
            else
            {

                if (movin)
                {
                    if (oldPos == Vector3.zero)
                    {
                        pined.RemoveAt(pined.Count - 1);
                        infos.unpinned[pinedId[pinedId.Count - 1]] = true;
                    }
                    else
                    {
                        int id = -1;
                        for (int i = 0; i < pined.Count; i++)
                        {
                            if (pined[i] = currentObject.gameObject)
                            {
                                id = i;
                            }
                        }

                        if (id >= 0)
                        {

                            RemoveLines(currentObject.gameObject);
                            

                            pined.RemoveAt(id);
                            if (pinedId[id] > 0)
                            {
                                infos.unlocked[id] = true;
                            }
                            pinedId.RemoveAt(id);
                        }
                    }
                    Destroy(currentObject.gameObject);
                    currentObject = null;
                    oldPos = Vector3.zero;
                    RefreshVisual();
                }
                else
                {
                    RemoveLastLine();
                }
            }
        }
    }

    void ObjectManipulation()
    {
        if (clicIn && movin)
        {
            PointerEventData pointerData = new PointerEventData(EventSystem.current);
            List<RaycastResult> results = new List<RaycastResult>();

            //Raycast using the Graphics Raycaster and mouse click position
            pointerData.position = Input.mousePosition;
            gr.Raycast(pointerData, results);

            if (currentObject != null)
            {
                if (results.Count > 0)
                {
                    Vector3 pos = results[0].worldPosition;
                    currentObject.position = pos;
                }
                else
                {
                    currentObject.position = Vector3.zero;
                }
            }
            
        }
    }

    void SetLinePos()
    {
        for (int i = 0; i < lines.Count; i++)
        {
            Vector3 pos0 = new Vector3(lineStarts[i].transform.position.x, lineStarts[i].transform.position.y, lineStarts[i].transform.position.z-.1f);
            Vector3 pos1;
            lines[i].SetPosition(0, pos0);

            if (lineEnds[i] == null)
            {
                PointerEventData pointerData = new PointerEventData(EventSystem.current);
                List<RaycastResult> results = new List<RaycastResult>();

                pointerData.position = Input.mousePosition;
                gr.Raycast(pointerData, results);

                if (results.Count > 0)
                {
                    pos1 = new Vector3(results[0].worldPosition.x, results[0].worldPosition.y, results[0].worldPosition.z - .1f);
                    //Vector3 pos = results[0].worldPosition;
                    lines[i].SetPosition(1, pos1);

                }
                else
                {
                    lines[i].SetPosition(1, Vector3.zero);
                }

            }
            else
            {
                pos1 = new Vector3(lineEnds[i].transform.position.x, lineEnds[i].transform.position.y, lineEnds[i].transform.position.z - .1f);
                lines[i].SetPosition(1, pos1);
            }
        }
    }

    void CheckMouseRemove()
    {
        if (Input.GetKeyDown(KeyCode.A) && !clicIn)
        {
            PointerEventData pointerData = new PointerEventData(EventSystem.current);
            List<RaycastResult> results = new List<RaycastResult>();

            //Raycast using the Graphics Raycaster and mouse click position
            pointerData.position = Input.mousePosition;
            gr.Raycast(pointerData, results);

            if (results.Count > 0)
            {
                GameObject result = results[0].gameObject;

                if (result.tag == "pin")
                {
                    RemoveLines(result);
                }
            }


        }
    }

    void RemoveLines(GameObject pinO)
    {
        List<int> removInt = new List<int>();

        for (int i = 0; i < lineStarts.Count; i++)
        {
            if (lineStarts[i] == pinO || lineEnds[i] == pinO)
            {
                removInt.Add(i);
            }
        }

        for (int i = removInt.Count - 1; i >= 0; i--)
        {
            RemoveLastLine();
        }


        
    }

    public void ActivateMenu()
    {
        menuOpen = !menuOpen;
        if (menuOpen)
        {
            if (lerpPos < 0)
            {
                lerpPos = 0;
            }
        }
        else
        {
            if (lerpPos >1)
            {
                lerpPos = 1;
            }
        }
        RefreshVisual();
    }

    void RefreshVisual()
    {

        infos.skipCount = 0;
        int count = 0;
        int realId = -1;
        for (int i = 0; i < infos.visual.Length; i++)
        {
            if(!infos.unlocked[i] || !infos.unpinned[i])
            {
                infos.skipCount++;
            }
        }
        maxPage = Mathf.FloorToInt((infos.visual.Length - infos.skipCount-1) / objectsPerPage);

        bool empty = false;

        for (int i = 0; i < menuImages.Length; i++)
        {
            if(currentPage * objectsPerPage + i+infos.skipCount < infos.visual.Length)
            {
                int targetCount = currentPage * objectsPerPage + i;

                count = -1;

                for (int j = 0; j < infos.unlocked.Length; j++)
                {
                    if(infos.unlocked[j] && infos.unpinned[j])
                    {
                        count++;
                    }

                    if (count == targetCount)
                    {
                        realId = j;
                        break;
                    }
                }
                if (realId >= 0)
                {
                    menuImages[i].sprite = infos.visual[realId];
                    realIds[i] = realId;
                    menuImages[i].color = infos.summon[realId].GetComponent<Image>().color;
                }
                else
                {
                    menuImages[i].sprite = null;
                    menuImages[i].color = Color.clear;
                    realIds[i] = -1;
                }
            }
            else
            {
                menuImages[i].sprite = null;
                menuImages[i].color = Color.clear;
                realIds[i] = -1;
                if (i == 0)
                {
                    empty = true;
                }
            }            
        }

        if (empty)
        {
            if (currentPage != 0)
            {
                ChangePage(-1);

            }
        }
    }

    public void ChangePage(int movement)
    {
        if (movement > 0)
        {
            if (currentPage< maxPage)
            {
                currentPage++;
            }
            else
            {
                currentPage = 0;
            }
        }
        else
        {
            if (currentPage > 0)
            {
                currentPage--;
            }
            else
            {
                currentPage = maxPage;
            }
        }

        RefreshVisual();
    }

    public void ChangeColor(int newCol)
    {
        currentColor = newCol;
    }

    public int FindIDInList(GameObject target)
    {
        int answer = -1;

        for (int i = 0; i < pined.Count; i++)
        {
            if (target == pined[i])
            {
                answer = i;
            }
        }

        return answer;
    }

    void RemoveLastLine()
    {
        Destroy(lines[lines.Count - 1].gameObject);
        lineStarts.RemoveAt(lineStarts.Count - 1);
        lineEnds.RemoveAt(lineEnds.Count - 1);
        lineStartsID.RemoveAt(lineStartsID.Count - 1);
        lineEndsID.RemoveAt(lineEndsID.Count - 1);
        lines.RemoveAt(lines.Count - 1);
        linesColor.RemoveAt(linesColor.Count - 1);
    }

    void LoadSetup()
    {
        LiegeData ld = SaveSystem.LoadLiege();

        if (ld != null)
        {
            for (int i = 0; i < ld.lineColors.Length; i++)
            {
                linesColor.Add(ld.lineColors[i]);
            }

            for (int i = 0; i < ld.positionsX.Length; i++)
            {
                GameObject temp;
                if (ld.objectsID[i] == -1)
                {
                    temp = Instantiate(pin, transform);
                }
                else
                {
                    temp = Instantiate(infos.summon[ld.objectsID[i]], transform);
                    infos.unpinned[ld.objectsID[i]] = false;
                }

                temp.transform.position = new Vector3(ld.positionsX[i], ld.positionsY[i], ld.positionsZ[i]);
                pined.Add(temp);
                
            }

            for (int i = 0; i < ld.lineStarts.Length; i++)
            {
                GameObject temp = Instantiate(lineRend, transform);
                LineRenderer tempLine = temp.GetComponent<LineRenderer>();
                lineStartsID.Add(ld.lineStarts[i]);
                lineEndsID.Add(ld.lineEnds[i]);
                lineStarts.Add(pined[lineStartsID[i]]);
                lineEnds.Add(pined[lineEndsID[i]]);
                lines.Add(tempLine);
                tempLine.startColor = tempLine.endColor = colors[linesColor[i]].col;


            }

            RefreshVisual();
        }
    }
}
