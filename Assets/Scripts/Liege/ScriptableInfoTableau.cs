﻿using UnityEngine.UI;
using UnityEngine;

[CreateAssetMenu(fileName = "Tableau", menuName = "ScriptableObjects/ScriptableTableau", order = 2)]
public class ScriptableInfoTableau : ScriptableObject
{

    public bool[] unlocked;
    public bool[] unpinned;
    public Sprite[] visual;
    public GameObject[] summon;
    public int skipCount = 0;

    public bool[] baseUnlocked;

    public void BaseStats()
    {
        unlocked = baseUnlocked;
        for (int i = 0; i < unpinned.Length; i++)
        {
            unpinned[i] = true;
        }
    }
}
