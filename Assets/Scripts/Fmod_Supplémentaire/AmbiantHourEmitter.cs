﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmbiantHourEmitter : MonoBehaviour
{

    [FMODUnity.EventRef]
    public string selectsound;
    public FMOD.Studio.EventInstance soundevent;

    public bool OverrideAttenuation = false;
    public float OverrideMinDistance = -1.0f;
    public float OverrideMaxDistance = -1.0f;

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, OverrideMaxDistance);
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, OverrideMinDistance);
    }

    // Start is called before the first frame update
    void Start()
    {
        soundevent = FMODUnity.RuntimeManager.CreateInstance(selectsound);
        FMODUnity.RuntimeManager.AttachInstanceToGameObject(soundevent, GetComponent<Transform>(), GetComponent<Rigidbody>());
        soundevent.start();
    }

    // Update is called once per frame
    void Update()
    {
        if (OverrideAttenuation)
        {
            soundevent.setProperty(FMOD.Studio.EVENT_PROPERTY.MINIMUM_DISTANCE, OverrideMinDistance);
            soundevent.setProperty(FMOD.Studio.EVENT_PROPERTY.MAXIMUM_DISTANCE, OverrideMaxDistance);
        }
    }
}
