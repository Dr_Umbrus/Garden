﻿using FMODUnity;
using FMOD.Studio;
using UnityEngine;
using UnityEngine.UI;

public class BusControl : MonoBehaviour
{
    
    Bus masterBus;
    [Header("Master")]
    public string masterBusPath;
    public Slider masterSlide;
    [Range(0f, 10f)] public float masterVolume;

    
    Bus musicBus;
    [Header("Music")]
    public string musicBusPath = "Music";
    public Slider musicSlide;
    [Range(0f, 10f)] public float musicVolume;


    Bus sdBus;
    [Header("Sound")]
    public string sdBusPath = "SoundEffect";
    public Slider sdSlide;
    [Range(0f, 10f)] public float sdVolume;

    

    // Start is called before the first frame update
    void Start()
    {

            

    }

    // Update is called once per frame
    void Update()
    {
        masterVolume = masterSlide.value;
        masterBus.setVolume(masterVolume / 10f);

        musicVolume = musicSlide.value;
        musicBus.setVolume(musicVolume / 10f);

        sdVolume = sdSlide.value;
        sdBus.setVolume(sdVolume / 10f);

    }
}