﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RadioPlay : MonoBehaviour
{

    [FMODUnity.EventRef]
    public string selectsound, clicEffect;
    public FMOD.Studio.EventInstance soundevent;
   

    public bool OverrideAttenuation = false;
    public float OverrideMinDistance = -1.0f;
    public float OverrideMaxDistance = -1.0f;

    public GameObject stratButton;
    public LeanTweenType phoneEnter;

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, OverrideMaxDistance);
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, OverrideMinDistance);
    }

    void Start()
    { 
        soundevent = FMODUnity.RuntimeManager.CreateInstance(selectsound);
       
    }

    void Update()
    {
      FMODUnity.RuntimeManager.AttachInstanceToGameObject(soundevent, GetComponent<Transform>(), GetComponent<Rigidbody>());

        if (OverrideAttenuation)
        { 
        soundevent.setProperty(FMOD.Studio.EVENT_PROPERTY.MINIMUM_DISTANCE, OverrideMinDistance);
        soundevent.setProperty(FMOD.Studio.EVENT_PROPERTY.MAXIMUM_DISTANCE, OverrideMaxDistance);
        }
    }

    void OnMouseDown()
    {
            
            FMOD.Studio.PLAYBACK_STATE fmodPbState;
            soundevent.getPlaybackState(out fmodPbState);
            if (fmodPbState != FMOD.Studio.PLAYBACK_STATE.PLAYING)
            {
            Debug.Log("On");
            FMODUnity.RuntimeManager.PlayOneShotAttached(clicEffect, gameObject);
            LeanTween.moveLocalZ(this.gameObject, 0.001979f, 0.05f).setEase(phoneEnter);
            soundevent.start();
            }
            else
            {
            Debug.Log("Off");
            FMODUnity.RuntimeManager.PlayOneShotAttached(clicEffect, gameObject);
            LeanTween.moveLocalZ(this.gameObject, 0.00204f, 0.05f).setEase(phoneEnter);
            soundevent.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
            }
    }

    public void ChangeFrequency(float newFreq)
    {
        soundevent.setParameterByName("radiofrequency", newFreq);
    }
}
