﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrosObjet : MonoBehaviour
{
    //script pour stocker la variable de taille pour les gros objets , sert quand on doit les déplacer a des positions données pour éviter
    //qu'il ne soit enfoncé dans les sols
    public float hauteur;
}
