﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectInspection : MonoBehaviour
{
    [SerializeField]
    //bool si mode inspection d objet activé
    public bool inspectiON = false;

    GameManager gm;

    //variable pour stocker l'objet en mouvement/en inspection
    public GameObject movingObject;
    //bool si objet en cours de déplacement
    public bool isMoving = false;
    //position de l'objet sur la table au moment ou il est sélectionné pour l'inspection
    public Vector3 prevPosInspection;
    //rotation de l'objet sur la table au moment ou il est sélectionné pour l'inspection
    public Vector3 prevRotaInspection;
    //array pour stocker les positions des points de vue/zooms en inspections
    public GameObject[] InspectionViews;
    //niveau de zoom actuel
    private int zoomLevel;
    //bool inspection possible
    public bool canInspect = true;

    //limite de la zone ou peuvent être déplacés les objets
    private Vector3 minPosTable = new Vector3(-0.200f, 0.03399992f, -3.55f);
    private Vector3 maxPosTable = new Vector3(0.300f, 0.03399992f, -2.45f);

    //canvas pendant l inspection d objet
    public Canvas InspectionUI;

    PickPhone pp;
    //Paramètres permettant d'avoir les sons de Zoom et dézoom son.
    [FMODUnity.EventRef]
    public string ZoomIn, ZoomOut;

    public ObjectListState ols;

    private void Awake()
    {
        gm = FindObjectOfType<GameManager>();
        pp = FindObjectOfType<PickPhone>();
    }


    // Update is called once per frame
    public void UpdateMe()
    {
        if (canInspect)
        {
            // permet la rotation de l'objet via les touches fléchés
            if (inspectiON && movingObject != null)
            {
                #region rotation avec touches
                if (Input.GetKey(KeyCode.UpArrow))
                {
                    movingObject.transform.Rotate(Vector3.forward * 200 * Time.deltaTime);
                }

                if (Input.GetKey(KeyCode.DownArrow))
                {
                    movingObject.transform.Rotate(-Vector3.forward * 200 * Time.deltaTime);
                }

                if (Input.GetKey(KeyCode.RightArrow))
                {
                    movingObject.transform.Rotate(-Vector3.up * 200 * Time.deltaTime);
                }

                if (Input.GetKey(KeyCode.LeftArrow))
                {
                    movingObject.transform.Rotate(Vector3.up * 200 * Time.deltaTime);
                }
                #endregion
                if (movingObject != null && pp.picked)
                {
                    endInspection();
                }
            }


            //action clic gauche
            //pour déplacer les objets sur le bureau (hors inspection)
            if (Input.GetMouseButtonDown(0) && !isMoving && !GameObject.Find("Interfaces").transform.GetChild(4).gameObject.activeSelf)
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                Physics.Raycast(ray, out hit);

                if (hit.collider != null)
                {
                    //définit l'objet qui est sélectionné / déplacé
                    if (hit.collider.tag == "ObjectInspection" && movingObject == null)
                    {
                        movingObject = hit.transform.gameObject;
                    }
                }

                if (!inspectiON)
                {
                    //sauvegarde la dernière position de l'objet lorsqu'il est sélectionné
                    if (hit.collider != null)
                    {
                        prevPosInspection = hit.transform.position;
                    }
                }
            }

            if (!pp.picked)
            {
                //permet de faire tourner l'objet inspecté avec la souris quand en mode inspection
                #region Rotation inspection
                if (Input.GetMouseButton(0))
                {

                    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                    RaycastHit hit;
                    Physics.Raycast(ray, out hit);
                    if (hit.collider != null)
                    {
                        if (inspectiON && !isMoving && hit.collider.gameObject.tag != "Outil")
                        {

                            //rotation avec la souris pour les gros objets
                            if (movingObject != null && movingObject.tag == "BigObject")
                            {
                                movingObject.transform.Rotate((Input.GetAxis("Mouse Y") * 200 * Time.deltaTime), (Input.GetAxis("Mouse X") * -200 * Time.deltaTime), 0, Space.World);
                            }
                            //rotation avec la souris pour les objets du bureau
                            if (movingObject != null && movingObject.tag == "ObjectInspection")
                            {
                                movingObject.transform.Rotate((Input.GetAxis("Mouse X") * 200 * Time.deltaTime), 0, (Input.GetAxis("Mouse Y") * 200 * Time.deltaTime), Space.World);
                            }

                        }
                    }
                }
                    
                #endregion

                //repose l'objet qui est déplacé par le joueur, remet a null l'objet avec lequel le joueur interagit
                if (Input.GetMouseButtonUp(0) && !GameObject.Find("Interfaces").transform.GetChild(4).gameObject.activeSelf)
                {
                    if (movingObject != null && !inspectiON)
                    {
                        //Si le joueur lache l'objet hors de la table, le replace au dernier endroit ou il était posé
                        if (movingObject.transform.position.x < minPosTable.x || movingObject.transform.position.x > maxPosTable.x ||
                            movingObject.transform.position.z < minPosTable.z || movingObject.transform.position.z > maxPosTable.z)
                        {
                            goBack();
                            movingObject = null;
                        }
                        //sinon , place l'objet sur la table à l'endroit indiqué
                        else
                        {
                            movingObject.transform.position = new Vector3(movingObject.transform.position.x, prevPosInspection.y, movingObject.transform.position.z);
                            movingObject = null;
                        }

                    }

                }

                // commande de zoom / dézoom sur un objet en mode inspection
                #region Zoom/dézoom
                if (Input.GetAxis("Mouse ScrollWheel") > 0f && zoomLevel != 1 && !isMoving)
                {
                    if (inspectiON && movingObject.tag != "BigObject")
                    {
                        zoomLevel++;
                        StartCoroutine(MoveInspecttion(movingObject.transform, InspectionViews[zoomLevel].transform.position, movingObject.transform.rotation, 0.5f, false));
                    }

                }

                if (Input.GetAxis("Mouse ScrollWheel") < 0f && zoomLevel != 0 && !isMoving)
                {
                    if (inspectiON && movingObject.tag != "BigObject")
                    {
                        zoomLevel--;
                        StartCoroutine(MoveInspecttion(movingObject.transform, InspectionViews[zoomLevel].transform.position, movingObject.transform.rotation, 0.5f, false));
                    }
                }
                #endregion

                //lance/arrete l'inspection d'un objet
                if (Input.GetMouseButtonDown(1) && !Input.GetMouseButton(0) && !isMoving && !GameObject.Find("Interfaces").transform.GetChild(4).gameObject.activeSelf)
                {
                    //avec un raycast
                    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                    RaycastHit hit;
                    Physics.Raycast(ray, out hit);
                    //lancement mode inspection
                    if (!inspectiON)
                    {
                        if (hit.collider != null)
                        {
                            if (hit.transform.gameObject.tag == "ObjectInspection" || hit.transform.gameObject.tag == "BigObject")
                            {
                                FMODUnity.RuntimeManager.PlayOneShotAttached(ZoomIn, gameObject);
                                if (!isMoving)
                                {
                                    movingObject = hit.transform.gameObject;
                                    prevPosInspection = hit.transform.position;
                                    prevRotaInspection = hit.transform.localEulerAngles;
                                    startInspection();
                                }
                            }
                        }
                    }

                    //quitte le mode inspection
                    else
                    {
                        if (inspectiON && !isMoving)
                        {
                            if (movingObject != null)
                            {
                                FMODUnity.RuntimeManager.PlayOneShotAttached(ZoomOut, gameObject);
                                endInspection();
                            }
                        }
                    }
                }
            }

        }

    }

    //fonction lancement de l'inspection
    public void startInspection()
    {
        //check si petit objet ou non afin de le rescale en moode inspection
        if (movingObject.GetComponent<PetitObjet>() != null)
        {
            movingObject.GetComponent<PetitObjet>().scaleUp();
        }
        //désactive les boutons de l'interface non utilisés en inspection
        GameObject.Find("Interface Général").transform.GetChild(1).gameObject.SetActive(false);
        GameObject.Find("Interface Général").transform.GetChild(2).gameObject.SetActive(false);

        InspectionUI.gameObject.SetActive(true);
        //check si gros objet ou non, et le replace en fonction
        if(movingObject.tag == "BigObject")
        {
            //check si l'objet a des angles custo ou non 
            if(movingObject.GetComponent<CustomAngles>() != null)
            {
                StartCoroutine(MoveInspecttion(movingObject.transform, GameObject.Find("GrosObjectInspection").transform.position, Quaternion.Euler(movingObject.GetComponent<CustomAngles>().StartInspAngle.x, movingObject.GetComponent<CustomAngles>().StartInspAngle.y, movingObject.GetComponent<CustomAngles>().StartInspAngle.z), 0.5f, true));
            }

            else
            {
                StartCoroutine(MoveInspecttion(movingObject.transform, GameObject.Find("GrosObjectInspection").transform.position, GameObject.Find("GrosObjectInspection").transform.rotation, 0.5f, true));
            }    
        }
        else
        {
            //check si l'objet a des angles custo ou non 
            if (movingObject.GetComponent<CustomAngles>() != null)
            {
                StartCoroutine(MoveInspecttion(movingObject.transform, InspectionViews[0].transform.position,Quaternion.Euler(movingObject.GetComponent<CustomAngles>().StartInspAngle.x, movingObject.GetComponent<CustomAngles>().StartInspAngle.y, movingObject.GetComponent<CustomAngles>().StartInspAngle.z), 0.5f, true));
            }

            else
            {
                StartCoroutine(MoveInspecttion(movingObject.transform, InspectionViews[0].transform.position, InspectionViews[0].transform.rotation, 0.5f, true));
            }
                
        }
        gm.uiS.ChangeFolderDescription();
    }

    //fonction fin de l'inspection
    public void endInspection()
    {
        InspectionUI.gameObject.SetActive(false);
        //check si petit objet ou non afin de le rescale en moode inspection
        if (movingObject.GetComponent<PetitObjet>() != null)
        {
            movingObject.GetComponent<PetitObjet>().scaleDown();
        }
        StopAllCoroutines();
        StartCoroutine(MoveInspecttion(movingObject.transform, prevPosInspection, Quaternion.Euler(prevRotaInspection), 0.5f, true));

        //réactive les boutons de l'interface
        GameObject.Find("Interface Général").transform.GetChild(1).gameObject.SetActive(true);
        GameObject.Find("Interface Général").transform.GetChild(2).gameObject.SetActive(true);
        //GameObject.Find("Interface Général").transform.GetChild(3).gameObject.SetActive(true);
        if (movingObject.name.Contains(ols.allObjects[0].name) && ols.currentBatch == 0)
        {
            FindObjectOfType<Telephone>().AddBatchNotif();
            //FindObjectOfType<ArchProgress>().nextBatch();
        }
        movingObject = null;
        zoomLevel = 0;
        if (gm.uiS.folderActive)
        {
            gm.uiS.ChangeFolderState();
        }
        //gm.uiS.ChangeFolderDescription();
        
    }

    //retour de l'objet a sa position précédente -> lorsque placé hors de la table ou en collision avec un autre objet présent
    public void goBack()
    {
        StartCoroutine(MoveInspecttion(movingObject.transform, prevPosInspection, movingObject.transform.rotation, 0.2f, false));
    }



    //fonction appelé par les boutons pour tourner l'objet a des angles prédéfinis
    public void RotaFunction(int idButon)
    {
        Quaternion rotation = new Quaternion(0,0,0,0);

        #region Angles des rotations
        //front
        if(idButon == 0)
        {
            
            rotation = Quaternion.Euler(movingObject.GetComponent<CustomAngles>().FrontAngle);
        }
        //back
        if (idButon == 1)
        {
            rotation = Quaternion.Euler(movingObject.GetComponent<CustomAngles>().BackAngle);
        }
        //bottom
        if (idButon == 2)
        {
            rotation = Quaternion.Euler(movingObject.GetComponent<CustomAngles>().BottomAngle);
        }
        //right
        if (idButon == 3)
        {
            rotation = Quaternion.Euler(movingObject.GetComponent<CustomAngles>().RightAngle);
        }
        //up
        if (idButon == 4)
        {
            rotation = Quaternion.Euler(movingObject.GetComponent<CustomAngles>().UpAngle);
        }
        //left
        if (idButon == 5)
        {
            rotation = Quaternion.Euler(movingObject.GetComponent<CustomAngles>().LeftAngle);
        }
        #endregion
        if (movingObject != null)
        {
            StopAllCoroutines();
            StartCoroutine(MoveInspecttion(movingObject.transform, movingObject.transform.position, rotation, 0.75f, false));
        }
    } 

    //coroutine pour bouger un objet inspecté     Objet a bouger        position cible        rotation cible      vitesse          bool pour indiquer si on sort du mode inspection
    public IEnumerator MoveInspecttion(Transform objectInspect, Vector3 position, Quaternion newRotation , float timeToMove, bool inspectMode)
    {

        #region Déplacement d'objets dans le cadre d'une inspection (un a la fois)
        if (!isMoving)
        {
            if (inspectMode)
            {
                isMoving = true;
                var currentPos = objectInspect.position;
                var t = 0f;
                //while pour le déplacement et rotation smooth
                while (t < 1)
                {
                    t += Time.deltaTime / timeToMove;
                    objectInspect.position = Vector3.Lerp(currentPos, position, t);
                    objectInspect.rotation = Quaternion.Lerp(objectInspect.transform.rotation, newRotation, t);
                    yield return null;
                }
                if (inspectMode)
                {
                    inspectiON = !inspectiON;
                }
                isMoving = false;
                if(!inspectiON)
                {
                    if(objectInspect.GetComponent<ObjectMove>() != null)
                    {
                        objectInspect.GetComponent<ObjectMove>().checkHauteur();
                    }     
                }   
            }
            
        }
        #endregion

        #region Déplacement d'objets sans inspection (plusieurs déplacements possibles simultanément)
        if (!inspectMode)
        {
            var currentPos = objectInspect.position;
            var t = 0f;
            //while pour le déplacement et rotation smooth
            while (t < 1)
            {
                t += Time.deltaTime / timeToMove;
                objectInspect.position = Vector3.Lerp(currentPos, position, t);
                objectInspect.rotation = Quaternion.Lerp(objectInspect.transform.rotation, newRotation, t);
                yield return null;
            }
            if (!inspectiON)
            {
                if(objectInspect.GetComponent<ObjectMove>() != null)
                {
                    objectInspect.GetComponent<ObjectMove>().checkHauteur();
                }
            }
        }
        #endregion
    }
}
