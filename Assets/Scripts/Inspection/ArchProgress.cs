﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ArchProgress : MonoBehaviour
{
    //script pour la progression du joueur permettant de faire venir les nouveaux batch d'objets lorsqu"il complète une section 
    GameManager gm;

    //GO pour stocker le batch a faire apparaitre et gérer ses déplacements en jeu
    public GameObject batch;
    public Vector3[] batchPos;

    public Vector3 spawnBatch;

    public bool currentlyBatch = false;

    //bool pour entrer/sortir du mode inspection d'objets
    public bool inspectiON;
    //bool d'état lorsqu'un objet en inspection est déplacé , afin d'éviter les multiples déplacements d'objets en inspection -> ne pas avoir plus d'un objet en mode inspection
    private bool isMoving;
    //int pour définir la position ou sera posé le prochain nouvel objet inspecté par le joueur
    private int idPosNewObject;
    //GO pour stocker l'objet actuellement manipulé par le joueur
    private GameObject movingObject;


    //list pour récupérer les objets actif sur le bureau, notamment pour les déplacer lors d'un nouvel arrivage d'objets
    public List<GameObject> activeObjects = new List<GameObject>();

    //Array pour stocker les positions ou sont déplacés les objets déja présents lors d'un arrivage d'objets
    public GameObject[] activeObjectsPos;
    //Array pour stocker les positions ou sont déplacés les nouveaux objets après inspection lors d'un arrivage d'objets
    public GameObject[] newObjectsPos;


    public ObjectListState ols;
    public Transform[] allObjectsPos;

    GameObject inter;
    public Vector3 storage;
    public Transform[] bigObjectsPos;

    public float hintCountdown;
    public float maxHintCountdown;
    public float secondHintCountdown;
    public float maxSecondHintCountdown;
    public bool openedHint = false;


    [FMODUnity.EventRef]
    public string placeholderTest, ZoomIn, ZoomOut;


    // Update is called once per frame
    public void UpdateMe()
    {
            //inspection des nouveaux objets du batch (code repris et modifié de ObjectInpection)
                #region Inspection nouveaux objets

                //lance l'inspection d'un objet du batch
                if (Input.GetMouseButtonDown(1) && gameObject.GetComponent<ObjectInspection>().canInspect == false)
                {
                    //avec un raycast
                    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                    RaycastHit hit;
                    Physics.Raycast(ray, out hit);

                    //lance le mode inspection
                    if (!inspectiON)
                    {
                        if (hit.collider != null)
                        {
                            if (hit.transform.gameObject.tag == "ObjectInspection" && !isMoving)
                            {
                                movingObject = hit.transform.gameObject;
                                //retire l'objet du batch
                                movingObject.transform.parent = null;
                                //check si petit objet ou non afin de le rescale en moode inspection
                                if (movingObject.GetComponent<PetitObjet>() != null)
                                {
                                    movingObject.GetComponent<PetitObjet>().scaleUp();
                                }

                                if (movingObject.GetComponent<CustomAngles>() != null)
                                {
                                    StartCoroutine(MoveInspection(movingObject.transform, GameObject.Find("InspectionView").transform.position, Quaternion.Euler(movingObject.GetComponent<CustomAngles>().StartInspAngle.x, movingObject.GetComponent<CustomAngles>().StartInspAngle.y, movingObject.GetComponent<CustomAngles>().StartInspAngle.z), 0.5f, true));
                                }

                                else
                                {
                                    StartCoroutine(MoveInspection(movingObject.transform, GameObject.Find("InspectionView").transform.position, GameObject.Find("InspectionView").transform.rotation, 0.5f, true));
                                }

                            }
                        }
                    }

                    //quitte le mode inspection
                    else
                    {
                        if (inspectiON && !isMoving)
                        {

                            if (movingObject != null)
                            {
                                gm.uiS.ChangeFolderDescription();
                                //check si petit ou gros objet ou non afin de le rescale en moode inspection ou de le replacer en quittant l inspection
                                if (movingObject.GetComponent<PetitObjet>() != null)
                                {
                                    movingObject.GetComponent<PetitObjet>().scaleDown();
                                }
                                if (movingObject.GetComponent<GrosObjet>() != null)
                                {
                                    StartCoroutine(MoveInspection(movingObject.transform, new Vector3(newObjectsPos[idPosNewObject].transform.position.x, movingObject.GetComponent<GrosObjet>().hauteur, newObjectsPos[idPosNewObject].transform.position.z), newObjectsPos[idPosNewObject].transform.rotation, 0.5f, true));
                                }
                                else
                                {
                                    StartCoroutine(MoveInspection(movingObject.transform, newObjectsPos[idPosNewObject].transform.position, new Quaternion(0, 0, 0, 1), 0.50f, true));
                                }
                                //dépose le nouvel objet sur le bureau a un endroit prédéfini
                                idPosNewObject++;
                                movingObject.tag = "Untagged";
                                movingObject = null;
                            }
                            //check si le batch est vide pour mettre fin à l inspection
                            if (batch.transform.childCount == 0)
                            {
                                currentlyBatch = false;
                                StartCoroutine(MoveInspection(batch.transform, new Vector3(0, 0, -9), batch.transform.rotation, 1f, false));
                                idPosNewObject = 0;
                                StartCoroutine(newBatchState());
                            }
                        }
                    }
                }
                #endregion

            if (batch.transform.position.z <= -9)
            {
                FMODUnity.RuntimeManager.PlayOneShotAttached(placeholderTest, gameObject);
                batch.transform.position = spawnBatch;
                for (int i = 0; i < activeObjects.Count; i++)
                {
                activeObjects[i].tag = "ObjectInspection";
                }
                gm.Save();
            }


            //active/désactive des collider pour empecher le joueur de sélectionner d autres objets que ceux du batch
           /* if (this.gameObject.GetComponent<ObjectInspection>().canInspect == false && !this.gameObject.GetComponent<RangementObjet>().storeMode)
            {
                GameObject.Find("ActualObjectsPos").GetComponent<BoxCollider>().enabled = true;
                GameObject.Find("NewObjectsPos").GetComponent<BoxCollider>().enabled = true;
            }
            else
            {
                GameObject.Find("ActualObjectsPos").GetComponent<BoxCollider>().enabled = false;
                GameObject.Find("NewObjectsPos").GetComponent<BoxCollider>().enabled = false;
            }*/


        //active/désactive le bouton pour faire venir le novueau batch, selon si ce dernier est disponible et si le joueur est bien sur le bureau
        //Les nouveaux batch se débloquent via le script DictionnaryControl


            //active/désactive l'Ui de présentation des nouveaux objets lorsqu'ils sont inspectés
            if (inspectiON)
            {
                if (Input.GetMouseButton(0))
                {

                    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                    RaycastHit hit;
                    Physics.Raycast(ray, out hit);

                    if (inspectiON && !isMoving && hit.collider.gameObject.tag != "Outil")
                    {

                        //rotation avec la souris pour les gros objets
                        if (movingObject != null && movingObject.tag == "BigObject")
                        {
                            movingObject.transform.Rotate((Input.GetAxis("Mouse Y") * 200 * Time.deltaTime), (Input.GetAxis("Mouse X") * -200 * Time.deltaTime), 0, Space.World);
                        }
                        //rotation avec la souris pour les objets du bureau
                        if (movingObject != null && movingObject.tag == "ObjectInspection")
                        {
                            movingObject.transform.Rotate((Input.GetAxis("Mouse X") * 200 * Time.deltaTime), 0, (Input.GetAxis("Mouse Y") * 200 * Time.deltaTime), Space.World);
                        }

                    }
                }
            }

            if (hintCountdown > 0)
            {
                hintCountdown -= Time.deltaTime;
            }
            else if (hintCountdown < 0)
            {
                hintCountdown = 0;
                gm.uiS.RemoveLock(true);
            ActivateSecondCountDown();
            }

            if (secondHintCountdown > 0 && openedHint)
            {
                secondHintCountdown -= Time.deltaTime;
            }
            else if (secondHintCountdown < 0 && openedHint)
            {
                secondHintCountdown = 0;
                gm.uiS.RemoveLock(false);
            }      
    }


    //récupère les objets actifs sur le bureau
    public int GetActiveObjects()
    {
        activeObjects.Clear();
        for (int i = 0; i < ols.shelfed.Length; i++)
        {
            if (!ols.shelfed[i] && ols.unlocked[i])
            {
                activeObjects.Add(allObjectsPos[i].gameObject);
            }
        }
        return activeObjects.Count;
    }



    //fais venir le prochain batch d'objets
    public void nextBatch()
    {
        if (ols.currentBatch == 1)
        {
           Tutoriel tuto =  FindObjectOfType<Tutoriel>();
            if (tuto != null)
            {
                tuto.EndTuto();
            }
        }
        openedHint = false;
        gm.uiS.NewBatch();
        hintCountdown = maxHintCountdown;
        secondHintCountdown = maxSecondHintCountdown;

        ols.currentBatch++;
        //désactive les boutons d'interfaces
        inter.transform.GetChild(0).gameObject.SetActive(false);
        inter.transform.GetChild(1).gameObject.SetActive(false);
        inter.transform.GetChild(2).gameObject.SetActive(false);
        //inter.transform.GetChild(3).gameObject.SetActive(false);

        //met fin a l'inspection d'objet si actif
        if (gameObject.GetComponent<ObjectInspection>().inspectiON && gameObject.GetComponent<ObjectInspection>().isMoving == false)
        {
            if(gameObject.GetComponent<ObjectInspection>().movingObject != null)
            {
                gameObject.GetComponent<ObjectInspection>().endInspection();
            }
        }
        //actualise la liste d'objets actifs, puis fait passer en mode "Inpsection nouveau Batch" via newBatchState
        StartCoroutine(newBatchState());

        batch.transform.position = Vector3.zero;

        int id = 0;
        for (int i = 0; i < ols.shelfed.Length; i++)
        {
            if (!ols.shelfed[i] && ols.unlocked[i] && ols.correspondingBatch[i]<ols.currentBatch)
            {
                StartCoroutine(MoveInspection(activeObjects[id].transform, activeObjectsPos[id].transform.position, new Quaternion(0, 0, 0, 1), 0.1f, false));
                activeObjects[id].tag = "Untagged";
                id++;
            }
        }

        List<GameObject> newObjects= new List<GameObject>();
        int j = 0;
        for (int i = 0; i < ols.correspondingBatch.Length; i++)
        {
            if (ols.correspondingBatch[i] == ols.currentBatch)
            {
                GameObject temp = Instantiate(ols.allObjects[i]);
                temp.transform.position = batchPos[j];
                temp.transform.parent = batch.transform;
                ols.unlocked[i]=true;
                allObjectsPos[i]=temp.transform;
                j++;
                newObjects.Add(temp);
            }
        }
        
        GetActiveObjects();

        for (int i = 0; i < ols.grosBatch.Length; i++)
        {
            if (ols.grosBatch[i] == ols.currentBatch)
            {
                GameObject temp = Instantiate(ols.grosObjects[i]);
                temp.transform.position = ols.grosPos[i];
            }
        }

        //fais venir le batch sur le bureau
        currentlyBatch = true;
        StartCoroutine(MoveInspection(batch.transform, new Vector3(0, batch.transform.position.y, -3.1f), batch.transform.rotation, 1f, false));
        gm.dc.AddWords();
    }

    public void ActivateSecondCountDown()
    {
        openedHint = true;
        secondHintCountdown = maxSecondHintCountdown;
    }

    void SaveData()
    {
        for (int i = 0; i < ols.positions.Length; i++)
        {
            if (allObjectsPos[i] != null)
            {
                if(allObjectsPos[i].tag== "ObjectRangement")
                {
                    ols.shelfed[i] = true;
                    ols.positions[i] = new Vector3(0, 0, -10);
                }
                else
                {
                    ols.shelfed[i] = false;
                    ols.positions[i] = allObjectsPos[i].position;
                    
                }
            }
        }
        SaveSystem.SavePositions(ols);
    }

    void LoadData()
    {
        PositionsData posD = SaveSystem.LoadPositions();

        if (posD == null)
        {
            ols.unlocked[0] = true;
            ols.shelfed[0] = false;
            for (int i = 1; i < ols.unlocked.Length; i++)
            {
                ols.unlocked[i] = false;
                ols.shelfed[i] = false;
            }
            ols.currentBatch = 0;
        }
        else
        {
            for (int i = 0; i < posD.posX.Length; i++)
            {
                ols.positions[i] = new Vector3(posD.posX[i], posD.posY[i], posD.posZ[i]);
                ols.unlocked[i] = posD.unlocked[i];
                ols.shelfed[i] = posD.shelfed[i];
                ols.currentBatch = posD.currentBatch;
            }
            
        }
        ols.positions[0] = new Vector3(-0.026f, 0.048f, -3.15f);
    }

    public void GameStart()
    {
        gm = FindObjectOfType<GameManager>();
        gm.ap = this;
        inter = GameObject.Find("Interface Général");
        spawnBatch = GameObject.Find("SpawnBatch").transform.localPosition;
        batch.transform.position = spawnBatch;
        batchPos = new Vector3[batch.transform.childCount];
        for (int i = batch.transform.childCount - 1; i >= 0; i--)
        {
            batchPos[i] = batch.transform.GetChild(i).transform.localPosition;
            Destroy(batch.transform.GetChild(i).gameObject);
        }
        allObjectsPos = new Transform[ols.positions.Length];

        LoadData();

        //List<GameObject> newObjects = new List<GameObject>();
        for (int i = 0; i < ols.positions.Length; i++)
        {
            if (ols.unlocked[i])
            {
                GameObject temp = Instantiate(ols.allObjects[i]);
                allObjectsPos[i] = temp.transform;

                if (ols.shelfed[i])
                {
                    ols.positions[i] = storage;
                    //ro.selectToStore.Add(temp);
                }
                temp.transform.position = ols.positions[i];
                //newObjects.Add(temp);
            }
        }
        for (int i = 0; i < ols.grosBatch.Length; i++)
        {
            if (ols.grosBatch[i] <= ols.currentBatch && ols.currentBatch > 0)
            {
                GameObject temp = Instantiate(ols.grosObjects[i]);
                temp.transform.position = ols.grosPos[i];
            }
        }
        //récupère la liste des objets actifs
        GetActiveObjects();
        gm.countObjects();
    }


    //coroutine pour les déplacements des objets
    public IEnumerator MoveInspection(Transform objectInspect, Vector3 position, Quaternion newRotation, float timeToMove, bool inspectMode)
    {
        #region Déplacement d'objets dans le cadre d'une inspection (un a la fois)
        if (!isMoving)
        {

            if (inspectMode)
            {
                inspectiON = !inspectiON;
                isMoving = true;
            }

            if (inspectiON && Input.GetMouseButtonDown(1))
                FMODUnity.RuntimeManager.PlayOneShotAttached(ZoomIn, gameObject);
            else if (!inspectiON && Input.GetMouseButtonDown(1))
                FMODUnity.RuntimeManager.PlayOneShotAttached(ZoomOut, gameObject);

            var currentPos = objectInspect.position;
            var t = 0f;
            //while pour le déplacement et rotation smooth
            while (t < 1)
            {
                t += Time.deltaTime / timeToMove;
                objectInspect.position = Vector3.Lerp(currentPos, position, t);

                objectInspect.rotation = Quaternion.Lerp(objectInspect.transform.rotation, newRotation, t);
                yield return null;
            }
            isMoving = false;
        }
        #endregion

        #region Déplacement d'objets sans inspection (plusieurs déplacements possibles simultanément)
        if (!inspectMode)
        {
            var currentPos = objectInspect.position;
            var t = 0f;
            //while pour le déplacement et rotation smooth
            while (t < 1)
            {
                t += Time.deltaTime / timeToMove;
                objectInspect.position = Vector3.Lerp(currentPos, position, t);
                objectInspect.rotation = Quaternion.Lerp(objectInspect.transform.rotation, newRotation, t);
                yield return null;
            }
        }
        #endregion
    }

    //démarre/met fin a l inspection du nouveau batch d objets -> désactive le script d'inspection/interaction d'objets standard
    public IEnumerator newBatchState()
    {
        yield return new WaitForSeconds(1f);
        
        //démarre la phase de nouveau Batch
        if (gameObject.GetComponent<ObjectInspection>().canInspect == true)
        {
            inter.transform.GetChild(0).gameObject.SetActive(true);
            inter.transform.GetChild(1).gameObject.SetActive(true);
            inter.transform.GetChild(2).gameObject.SetActive(true);
            //inter.transform.GetChild(3).gameObject.SetActive(true);
            gameObject.GetComponent<ObjectInspection>().canInspect = false;
        }

        //termine la phase de nouveau batch
        else
        {
            gameObject.GetComponent<ObjectInspection>().canInspect = true;
            gm.countObjects();
            if (ols.currentBatch != 1)
            { 
            gm.Transition(ols.currentBatch, false);
            }
        }
        
    }
}
