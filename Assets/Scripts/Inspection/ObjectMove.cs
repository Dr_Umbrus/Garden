﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class ObjectMove : MonoBehaviour
{

    //script pour déplacer l'objet avec la souris lorsque n'est pas en mode inspection
    //Ne fonctionne que si le script ObjectInspection est enable -> ne fonctionne pas quand on inspecte un nouveau batch/sur les objets rangés/sur les gros objets

    //coordonnées en vecteur de la souris
    private Vector3 mOffset;
    //coordonées en float de la souris
    private float mZCoord;
    //script inspection d'objet pour check son état
    public ObjectInspection inspection;
    //script état téléphone
    public Telephone phoneScript;

    //Son quand l'objet est pris ou posé
    [FMODUnity.EventRef]
    public string posedSound;

    private void Start()
    {
        //récupère le script de gestion de l'inspection
        inspection = GameObject.Find("GameManager").GetComponent<ObjectInspection>();
        phoneScript = GameObject.Find("Phone").GetComponent<Telephone>();
    }

    private void Update()
    {
        if(inspection.canInspect == true && phoneScript.state == Telephone.PhoneStates.Sleep)
        {
            //check si un objet est sous l'objet déplacé , pour éviter les overlap
            RaycastHit hit;
            Physics.Raycast(this.transform.position, -this.transform.up, out hit);
            Debug.DrawRay(this.transform.position, -this.transform.up);
            if (Input.GetMouseButtonUp(0))
            {
                    if (hit.transform != null)
                    {
                        this.GetComponent<BoxCollider>().enabled = false;
                        if (hit.transform.gameObject.tag == "ObjectInspection" && inspection.movingObject != null && inspection.inspectiON == false)
                        {
                            inspection.goBack();
                            inspection.movingObject = null;
                        }
                        this.GetComponent<BoxCollider>().enabled = true;
                    }
            }
        }
    }


    void OnMouseDown()
    {
        if(inspection.canInspect == true && phoneScript.state == Telephone.PhoneStates.Sleep)
        {
            //check si l'objet n'est pas en inspection et permet de le déplacer
            if (inspection.inspectiON == false && inspection.isActiveAndEnabled && !inspection.isMoving && !GameObject.Find("Interfaces").transform.GetChild(4).gameObject.activeSelf)
            {
                FMODUnity.RuntimeManager.PlayOneShotAttached(posedSound, gameObject);
                mZCoord = Camera.main.WorldToScreenPoint(gameObject.transform.position).z;
                // Store offset = gameobject world pos - mouse world pos
                mOffset = gameObject.transform.position - GetMouseAsWorldPoint();
            }
        }
        
        
    }

    void OnMouseUp()
    {
        if(inspection.canInspect == true && phoneScript.state == Telephone.PhoneStates.Sleep)
        {
            if (inspection.inspectiON == false && inspection.isActiveAndEnabled && !inspection.isMoving && !GameObject.Find("Interfaces").transform.GetChild(4).gameObject.activeSelf)
            {
                //Son se produisant quand on lâche l'objet
                FMODUnity.RuntimeManager.PlayOneShotAttached(posedSound, gameObject);
            }
        }
    }

    //récupère la position de la souris dans le monde en convertissant sa position
    private Vector3 GetMouseAsWorldPoint()
    {
        // Pixel coordinates of mouse (x,y)
        Vector3 mousePoint = Input.mousePosition;
        // z coordinate of game object on screen
        mousePoint.z = mZCoord;
        // Convert it to world points
        return Camera.main.ScreenToWorldPoint(mousePoint);
    }

    //fonction pour check si un objet n est pas en lévitation au dessus de la table
    //WIP Ajouter les hauteurs perosnnalisés pour tout les objets
    public void checkHauteur()
    {
        if(this.gameObject.GetComponent<GrosObjet>() != null)
        {
            if (this.gameObject.tag == "ObjectInspection" && !inspection.isMoving && this.transform.position.y != this.gameObject.GetComponent<GrosObjet>().hauteur)
            {
                this.transform.position = new Vector3(this.transform.position.x, this.gameObject.GetComponent<GrosObjet>().hauteur , this.transform.position.z);
            }
        }
        else
        {
            if (this.gameObject.tag == "ObjectInspection" && !inspection.isMoving && this.transform.position.y != 0)
            {
                this.transform.position = new Vector3(this.transform.position.x, 0, this.transform.position.z);
            }
        }
    }


    //actualise la position de l'objet en fonction de la souris (Déplacement)
    void OnMouseDrag()
    {
        if(inspection.canInspect == true && phoneScript.state == Telephone.PhoneStates.Sleep)
        {
            if (!inspection.isMoving && !GameObject.Find("Interfaces").transform.GetChild(4).gameObject.activeSelf)
            {
                if (inspection.inspectiON == false && inspection.isActiveAndEnabled && this.gameObject.tag == "ObjectInspection")
                {
                    if (this.gameObject.GetComponent<GrosObjet>() != null)
                    {
                        transform.position = new Vector3(GetMouseAsWorldPoint().x + mOffset.x, this.gameObject.GetComponent<GrosObjet>().hauteur + 0.05f, GetMouseAsWorldPoint().z + mOffset.z);
                    }
                    else
                    {
                        transform.position = new Vector3(GetMouseAsWorldPoint().x, 0.1f, GetMouseAsWorldPoint().z + mOffset.z);
                    }
                }
            }
        }
    }

}