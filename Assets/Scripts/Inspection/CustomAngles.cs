﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomAngles : MonoBehaviour
{
    public Vector3 StartInspAngle;

    public Vector3 FrontAngle;
    public Vector3 BackAngle;
    public Vector3 UpAngle;
    public Vector3 BottomAngle;
    public Vector3 RightAngle;
    public Vector3 LeftAngle;
}