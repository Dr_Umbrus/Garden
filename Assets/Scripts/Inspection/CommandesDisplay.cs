﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class CommandesDisplay : MonoBehaviour
{
    private bool isDisplayed;
    public GameObject commandes;
    public GraphicRaycaster gr;


    // Update is called once per frame
    void Update()
    {

        if(commandes.activeSelf && Input.GetMouseButtonDown(0))
        {
            PointerEventData pointerData = new PointerEventData(EventSystem.current);
            List<RaycastResult> results = new List<RaycastResult>();

            //Raycast using the Graphics Raycaster and mouse click position
            pointerData.position = Input.mousePosition;
            gr.Raycast(pointerData, results);

            if (results.Count > 0)
            {
                bool ok = false;
                for (int i = 0; i < results.Count; i++)
                {
                    if (results[i].gameObject.tag == "commandes")
                    {
                        ok = true;
                    }
                }
                if (ok == false)
                {

                    Debug.Log("a");
                    displayCommandes();
                }
            }
            else
            {
                Debug.Log("b");
                displayCommandes();
            }
        }
    }

    public void displayCommandes()
    {
        if (!isDisplayed)
        {
            commandes.SetActive(true);
        }

        else
        {
            commandes.SetActive(false);
        }

        isDisplayed = !isDisplayed;
    }
}
