﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OutilsMove : MonoBehaviour
{
    //script pour déplacer les outils, n'est actuellement pas soumis au memes règles que les objets du bureau
    //déplaçable en permanence pour le moment

    private Vector3 mOffset;
    private float mZCoord;
    private ObjectInspection inspection;

    private void Start()
    {
        //récupère le script de gestion de l'inspection
        inspection = GameObject.Find("GameManager").GetComponent<ObjectInspection>();
    }

    void OnMouseDown()
    {
            mZCoord = Camera.main.WorldToScreenPoint(
            gameObject.transform.position).z;
            // Store offset = gameobject world pos - mouse world pos
            mOffset = gameObject.transform.position - GetMouseAsWorldPoint();
    }

    //récupère la position de la souris dans le monde en convertissant sa position
    private Vector3 GetMouseAsWorldPoint()
    {
        // Pixel coordinates of mouse (x,y)
        Vector3 mousePoint = Input.mousePosition;
        // z coordinate of game object on screen
        mousePoint.z = mZCoord;
        // Convert it to world points
        return Camera.main.ScreenToWorldPoint(mousePoint);
    }


    //actualise la position de l'objet en fonciton de la souris
    void OnMouseDrag()
    {
        transform.position = new Vector3(GetMouseAsWorldPoint().x, this.gameObject.transform.position.y, GetMouseAsWorldPoint().z + mOffset.z);
    }

}