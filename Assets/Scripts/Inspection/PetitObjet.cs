﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PetitObjet : MonoBehaviour
{
    //script spéciale a placer sur les objets plus petits afin de les grossir lors de l inspection pour qu'ils aient la meme taille a l'oeil nu que les autres objets


    private ObjectInspection inspection;

    //vector3 a définir dans l inspecteur pour chaque objet en fonciton de leur taille (recommandé pour les plus petits -> 10 10 10)
    public Vector3 scaleInspection;
    public Vector3 initialScale = new Vector3(1, 1, 1);
    // Start is called before the first frame update
    void Start()
    {
        inspection = GameObject.Find("GameManager").GetComponent<ObjectInspection>();
    }

    //fonction pour grossir l'objet
    public void scaleUp()
    {
        StopAllCoroutines();
        StartCoroutine(reScaleObject(this.transform, scaleInspection, 0.50f));
    }

    //fonction pour réduire l'objet
    public void scaleDown()
    {
        StopAllCoroutines();
        StartCoroutine(reScaleObject(this.transform, initialScale, 0.50f));
    }

    //coutourine pour faire changer l'objet de manière smooth
    public IEnumerator reScaleObject(Transform objectInspect, Vector3 newScale,float timeToMove)
    {
            var currentScale = objectInspect.localScale;
            var t = 0f;
            while (t < 1)
            {
                t += Time.deltaTime / timeToMove;
                objectInspect.localScale= Vector3.Lerp(currentScale, newScale, t);
                yield return null;
            }
    }
}
