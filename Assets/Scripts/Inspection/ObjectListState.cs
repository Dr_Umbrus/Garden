﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Object List", menuName = "ScriptableObjects/ObjectList", order = 4)]
public class ObjectListState : ScriptableObject
{

    [Header("SaveThis")]
    public bool[] unlocked;
    public Vector3[] positions;
    public bool[] shelfed;
    public int currentBatch;

    [Header("Objets")]
    public GameObject[] allObjects;
    public Sprite[] photos;
    [TextArea(15, 35)]
    public string[] descriptions;
    [TextArea(3, 35)]
    public string[] appDescriptions;
    public int[] correspondingBatch;

    [Header("Gros")]
    public GameObject[] grosObjects;
    public int[] grosBatch;
    public Vector3[] grosPos;
    [TextArea(8, 35)]
    public string[] grosDescription;
    public Sprite[] grosPhotos;

    [Header("Batch")]
    [TextArea(20, 35)]
    public string[] batchDescriptions;
    public Sprite batchPhotos;

    [Header("Hints")]
    [TextArea(10, 25)]
    public string[] firstHint;
    [TextArea(10, 25)]
    public string[] secondHint;

}
