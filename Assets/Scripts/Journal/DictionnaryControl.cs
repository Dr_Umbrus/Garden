﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public class DictionnaryControl : MonoBehaviour
{
    
    GameManager gm;
    ObjectListState ols;
    // Data input from the Unity inspector
    public List<Sprite> listWords;          // All of the images will be associated with the equivalent numbers from listMeanings
    public List<int> correspondingBatch;
    public List<string> listMeanings;       // French equivalent of all of the words in listWords
    public List<string> listExtraMeanings;  // Extra words that don't have an equivalent in the langage, is meant to confuse the player.
    public List<int> correspondingExtraBatch;

    public int[] unlockNextBatch;
    public int numberOfValidated = 0;
    // Data managed internally with the given information
    [HideInInspector]
    public List<Word> listDictionnary;      // This holds every word/meaning association
    [HideInInspector]
    public List<Pairing> listJournal;       // This holds every pairing
    [HideInInspector]
    public List<Pairing> listKnownJournal;  // This will manage every word known by the player, and how they're displayed

    //[HideInInspector]
    public List<string> totalMeanings;      // Every possible meaning (will get sorted alphabetically)

    [HideInInspector]
    public int currentPage;                 // The first page will be 0, to align with the list, and will always be displayed as currentPage+1 for the player, as books don't start from page 0

    // Related to newCorrect/notifications about new words being validated
    [HideInInspector]
    public bool newCorrectBefore;
    [HideInInspector]
    public bool newCorrectAfter;
    [HideInInspector]
    public float[] newCorrectFade;
    public Image newCorrectNotification;

    // Additional references
    public GameObject journalCanvas;
    public GameObject[] pageElement;
    public Image[] wordImages;
    public TextMeshProUGUI pageNumberLeft;
    public TextMeshProUGUI pageNumberRight;
    public GameObject journalObject;        // This is the physical object on the desk, that opens the journal menu when clicked
    [HideInInspector]
    public float journalOpening = 0.0f;     // This is between 0 and 1, the first half dictates pulling the journal from the desk, and the second half showing it in front 
    private float initialJournalPosX;
    private Vector3 initialJournalUI = Vector3.zero;

    //[HideInInspector]
    public List<string> removedWords = new List<string>();

    [Header("FMOD")]
    [FMODUnity.EventRef]
    public string openBook;
    [FMODUnity.EventRef]
    public string closeBook;
    [FMODUnity.EventRef]
    public string switchPage;
    [FMODUnity.EventRef]
    public string validationWord;

    public GraphicRaycaster gr;
    public GameObject tuto;
    public int tutoState=-1;


    private void Awake()
    {
        for (int i = 0; i < wordImages.Length; i++)
        {
            wordImages[i].material = new Material(wordImages[i].material);
        }
    }
    // Start is called before the first frame update
    public void GameStart()
    {
        gm = FindObjectOfType<GameManager>();
        ols = gm.ols;
        gm.dc = this;
        listDictionnary = new List<Word>();

        // Get all of the information to populate the listDictionnary
        for (int i = 0; i < listWords.Count; i++)
        {
            listDictionnary.Add(new Word(listWords[i], listMeanings[i]));
        }

        
        // Use this information to populate the listJournal
        listJournal = new List<Pairing>();
        for (int j = 0; j < listDictionnary.Count; j++)
        {
            listJournal.Add(new Pairing(j, listDictionnary[j]));
        }
        
        // Initialize the listKnownJournal
        listKnownJournal = new List<Pairing>();

        // Initialize the rest of the values
        currentPage = 0;


        // Fill the meaning dropdowns
        totalMeanings.Clear();
        for (int k = 0; k < listExtraMeanings.Count; k++)
        {
            if (correspondingExtraBatch[k] == ols.currentBatch)
                totalMeanings.Add(listExtraMeanings[k]);
        }
        for (int i = 0; i < correspondingBatch.Count; i++)
        {
            if (correspondingBatch[i] == ols.currentBatch)
            {
                totalMeanings.Add(listMeanings[i]);
            }
            else if (correspondingBatch[i] < ols.currentBatch)
            {
                bool test = false;
                for (int j = 0; j < removedWords.Count; j++)
                {
                    if (listMeanings[i] == removedWords[j])
                    {
                        test = true;
                    }
                }
                if (!test)
                    totalMeanings.Add(listMeanings[i]);

            }
        }

        // Order the list alphabetically
        totalMeanings.Sort();
        // Add an empty meaning
        totalMeanings.Insert(0, "");
        // Then add it to all of the dropdown options one by one

        for (int l = 0; l < pageElement.Length; l++)
        {
            pageElement[l].transform.GetChild(1).GetComponent<TMP_Dropdown>().ClearOptions();
            pageElement[l].transform.GetChild(1).GetComponent<TMP_Dropdown>().AddOptions(totalMeanings);
        }

        // Notifications
        newCorrectBefore = false;
        newCorrectAfter = false;
        newCorrectFade = new float[6];
        for(int m = 0; m < newCorrectFade.Length; m++)
        {
            newCorrectFade[m] = 0;
        }

        initialJournalPosX = journalObject.transform.position.x;

        

        // Once initialized, disable the canvas
        journalCanvas.SetActive(false);
        LoadData();
    }


    // Update is called once per frame
    public void UpdateMe()
    {

        if (journalCanvas.activeSelf==true)
        {
            if (Input.GetMouseButtonDown(0))
            {
                PointerEventData pointerData = new PointerEventData(EventSystem.current);
                List<RaycastResult> results = new List<RaycastResult>();

                //Raycast using the Graphics Raycaster and mouse click position
                pointerData.position = Input.mousePosition;
                gr.Raycast(pointerData, results);

                if (results.Count > 0)
                {
                    bool nope = false;
                    bool ok = false;
                    for (int i = 0; i < results.Count; i++)
                    {
                        if (results[i].gameObject.tag == "carnet")
                        {
                            ok = true;
                        }
                        if (results[i].gameObject.tag == "noCarnet")
                        {
                            nope = true;
                        }
                    }
                    if (ok == false || nope)
                    {
                        gm.OpenJournal();
                    }
                }
                else
                {
                    gm.OpenJournal();
                }
            }
        }
        else
        {
            // Attempt to open the journal by clicking on it
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Input.GetMouseButtonDown(0))
            {
                if (Physics.Raycast(ray, out hit))
                {
                    if (hit.collider.gameObject == journalObject)
                    {
                        gm.OpenJournal();
                    }
                }
            }
        }

        // Manage displacement of the journal menu/object on the desk when opening/closing it
        if (journalCanvas.activeSelf)
        {
            journalOpening += Time.deltaTime;
            if (journalOpening > 1.0f)
            {
                journalOpening = 1.0f;
            }
        }
        else
        {
            journalOpening -= Time.deltaTime;
            if (journalOpening < 0.0f)
            {
                journalOpening = 0.0f;
            }
        }

        if(initialJournalUI == Vector3.zero)
        {
            initialJournalUI = journalCanvas.transform.GetChild(0).GetComponent<RectTransform>().transform.position;
        }
        // Move both of them

        journalObject.transform.position = new Vector3(initialJournalPosX + (journalOpening * 2), journalObject.transform.position.y, journalObject.transform.position.z);

        // CELLE LA QUI MARCHE PAS
        //journalCanvas.transform.GetChild(0).transform.position = new Vector3(journalCanvas.transform.GetChild(0).transform.position.x, initialJournalUI.y - ((1 - journalOpening) * 500), 0);


    }


    // Functions

    // This function is called to toggle the journal when called
    public void OpenJournal()
    {
        Tutoriel tutoText = tuto.GetComponent<Tutoriel>();
        //journalCanvas.SetActive(!journalCanvas.activeSelf);
        if (journalCanvas.activeSelf)
        {
            if (tutoState < tutoText.maxStep)
            {

                if (tutoText != null)
                {
                    tutoText.UpdateCarnet();
                    tutoState = tutoText.tutoStep;
                }
                else
                {
                    tutoState = tutoText.maxStep;
                }


            }
            tuto.SetActive(false);
        }
        else
        {
            if (tutoState < tutoText.maxStep)
            {
                tuto.SetActive(true);
            }
        }

        GameObject GM = GameObject.Find("GameManager").gameObject;
        GameObject InterfaceG = GameObject.Find("Interface Général").gameObject;

        if (GM.GetComponent<ArchProgress>().inspectiON)
        {
            GM.GetComponent<ObjectInspection>().endInspection();
        }
        //Set active/inactive the other buttons of the general interface if the journal is active/inactive
        if (journalCanvas.activeSelf)
        {
            FMODUnity.RuntimeManager.PlayOneShotAttached(openBook, gameObject);

            InterfaceG.transform.GetChild(1).gameObject.SetActive(false);
            InterfaceG.transform.GetChild(2).gameObject.SetActive(false);

            //désactive le script et l'interface d'inspection 
            if (GM.GetComponent<ObjectInspection>().inspectiON && !GM.GetComponent<ObjectInspection>().isMoving)
            {
                GM.GetComponent<ObjectInspection>().enabled = false;
                GameObject.Find("Interfaces").transform.GetChild(2).gameObject.SetActive(false);
            }
        }
        else
        {
            FMODUnity.RuntimeManager.PlayOneShotAttached(closeBook, gameObject);
            InterfaceG.transform.GetChild(1).gameObject.SetActive(true);
            InterfaceG.transform.GetChild(2).gameObject.SetActive(true);

            //active le script et l'interface d'inspection 
            if (GM.GetComponent<ObjectInspection>().inspectiON && !GM.GetComponent<ObjectInspection>().isMoving)
            {
                GM.GetComponent<ObjectInspection>().enabled = true;
                GameObject.Find("Interfaces").transform.GetChild(2).gameObject.SetActive(true);
            }
        }
        SetRoundAmount();
        UpdateCurrentPage();
    }

    // This function is called when a playerMeaning is changed, to check for 3 correct meanings and update the list
    public void CheckCorrect()
    {
        // Since this new meaning is correct, check through the whole journal to see if there are 3 correct meanings, and if so, put them all to "correct"
        int c1 = -1;
        int c2 = -1;

        for(int i = 0; i < listKnownJournal.Count; i++)
        {

            if ((listKnownJournal[i].playerMeaning == listKnownJournal[i].word.meaning) && (!listKnownJournal[i].correct))
            {
                // This word is correct so we record the index to the first available variable
                if (c1 == -1)
                {
                    c1 = i;
                }
                else if (c2 == -1)
                {
                    c2 = i;
                }
                else
                {
                    // We look no further, we already have the three correct meanings. Toggle all of them to true, as well as the "newCorrect" to trigger the animation
                    listKnownJournal[c1].correct = true;
                    listKnownJournal[c1].newCorrect = true;
                    listKnownJournal[c2].correct = true;
                    listKnownJournal[c2].newCorrect = true;
                    listKnownJournal[i].correct = true;
                    listKnownJournal[i].newCorrect = true;
                    FMODUnity.RuntimeManager.PlayOneShotAttached(validationWord, gameObject);
                    removedWords.Add(totalMeanings[totalMeanings.IndexOf(listKnownJournal[c1].playerMeaning)]);
                    removedWords.Add(totalMeanings[totalMeanings.IndexOf(listKnownJournal[c2].playerMeaning)]);
                    removedWords.Add(totalMeanings[totalMeanings.IndexOf(listKnownJournal[i].playerMeaning)]);

                    // Get the correct meanings out of the possible meanings for the other words
                    totalMeanings.RemoveAt(totalMeanings.IndexOf(listKnownJournal[c1].playerMeaning));
                    totalMeanings.RemoveAt(totalMeanings.IndexOf(listKnownJournal[c2].playerMeaning));
                    totalMeanings.RemoveAt(totalMeanings.IndexOf(listKnownJournal[i].playerMeaning));

                    // Then add it to all of the dropdown options one by one
                    for (int l = 0; l < pageElement.Length; l++)
                    {
                        pageElement[l].transform.GetChild(1).GetComponent<TMP_Dropdown>().ClearOptions();
                        pageElement[l].transform.GetChild(1).GetComponent<TMP_Dropdown>().AddOptions(totalMeanings);
                    }

                    //Active la notif pour des nouveaux objets
                    //GameObject.Find("Interface Général").transform.GetChild(3).gameObject.SetActive(true);

                    UpdateCurrentPage();
                    numberOfValidated += 3;
                    if (numberOfValidated >= unlockNextBatch[ols.currentBatch] && ols.currentBatch>0)
                    {
                        FindObjectOfType<Telephone>().AddBatchNotif();
                        if (ols.currentBatch == 3)
                        {
                            gm.Transition(2, true);
                        }
                        else if (ols.currentBatch == 6) gm.Transition(3, true);
                    }

                    for (int j = 0; j < pageElement.Length; j++)
                    {
                        if ((currentPage * 6) + j < listKnownJournal.Count)
                        {

                            // If this is a newly validated word, play the animation transition
                            if (listKnownJournal[(currentPage * 6) + j].newCorrect)
                            {
                                StartCoroutine(Dissolve(j));
                                listKnownJournal[(currentPage * 6) + j].newCorrect = false;
                            }
                        }
                    }
                }
            }
        }

        // If we went through the whole loop and found less than 3 correct meanings : we do nothing.

        gm.Save();
    }


    public void AddWords()
    {
        listKnownJournal.Clear();
        for (int i = 0; i < listJournal.Count; i++)
        {
            if (correspondingBatch[i] <= ols.currentBatch)
            {
                listKnownJournal.Add(listJournal[i]);
            }
        }

        // Fill the meaning dropdowns
        totalMeanings.Clear();
        for (int k = 0; k < listExtraMeanings.Count; k++)
        {
            if (correspondingExtraBatch[k] == ols.currentBatch)
                totalMeanings.Add(listExtraMeanings[k]);
        }
        for (int i = 0; i < correspondingBatch.Count; i++)
        {
            if (correspondingBatch[i] == ols.currentBatch)
            {
                totalMeanings.Add(listMeanings[i]);
            }

            else if (correspondingBatch[i] < ols.currentBatch)
            {
                bool test = false;
                for (int j = 0; j < removedWords.Count; j++)
                {
                    if (listMeanings[i] == removedWords[j])
                    {
                        test = true;
                    }
                }

                if (!test)
                    totalMeanings.Add(listMeanings[i]);

            }
        }

        // Order the list alphabetically
        totalMeanings.Sort();
        // Then add it to all of the dropdown options one by one

        List<string> optionMeanings = new List<string>(totalMeanings);

        for (int i = 0; i < removedWords.Count; i++)
        {
            for (int j = 0; j < totalMeanings.Count; j++)
            {
                if (totalMeanings[j] == removedWords[i])
                {
                    optionMeanings.RemoveAt(j);
                    break;
                }

            }
        }

        // Add an empty meaning
        totalMeanings.Insert(0, "");
        optionMeanings.Insert(0, "");

        for (int l = 0; l < pageElement.Length; l++)
        {
            pageElement[l].transform.GetChild(1).GetComponent<TMP_Dropdown>().ClearOptions();
            pageElement[l].transform.GetChild(1).GetComponent<TMP_Dropdown>().AddOptions(optionMeanings);
        }
    }

    // Button functions
    public void previousPage()
    {
        FMODUnity.RuntimeManager.PlayOneShotAttached(switchPage, gameObject);

        currentPage = Mathf.Clamp(currentPage - 1, 0, (int)Mathf.Ceil((listKnownJournal.Count - 1) / 6));
      //  pageNumberLeft.text = "Page " + ((2 * currentPage) + 1);
      //  pageNumberRight.text = "Page " + ((2 * currentPage) + 2);
        SetRoundAmount();
        UpdateCurrentPage();
    }

    public void nextPage()
    {
        FMODUnity.RuntimeManager.PlayOneShotAttached(switchPage, gameObject);

        currentPage = Mathf.Clamp(currentPage + 1, 0, (int)Mathf.Ceil((listKnownJournal.Count-1) / 6));
       // pageNumberLeft.text = "Page " + ((2 * currentPage) + 1);
       // pageNumberRight.text = "Page " + ((2 * currentPage) + 2);
        SetRoundAmount();
        UpdateCurrentPage();
    }

    
    public void UpdateDropdown(int index)
    {
        // Get the label
        int newMeaning = pageElement[index].transform.GetChild(1).GetComponent<TMP_Dropdown>().value;

        listKnownJournal[(currentPage * 6) + index].playerMeaning = totalMeanings[newMeaning];
        CheckCorrect();
    }

    

    // Visual-related function
    public void UpdateCurrentPage()
    {

        // This will do most of the heavy lifting for displaying the correct words at the correct place
        // First enable/disable the elements that are not needed (incomplete last page)
        for(int i = 0; i < pageElement.Length; i++)
        {
            if((currentPage*6) + i < listKnownJournal.Count)
            {
                pageElement[i].SetActive(true);

                // Then fill the correct informations on the page
                // Set the word image 
                wordImages[i].sprite = listKnownJournal[(currentPage * 6) + i].word.word;

                /*// If this is a newly validated word, play the animation transition
                if(listKnownJournal[(currentPage * 6) + i].newCorrect)
                {
                    Debug.Log("coucou");
                    // PLAY THE ANIMATION/TRANSITION HERE
                    newCorrectFade[i] = 0.01f;      // The automatic increase won't be triggered unless it is above 0, so we start it here

                    // Disable newCorrect, it won't be used anymore
                    listKnownJournal[(currentPage * 6) + i].newCorrect = false;
                }
                else if(newCorrectFade[i]>=1 || newCorrectFade[i]<=0)
                {
                    if(listKnownJournal[(currentPage * 6) + i].correct)
                    {
                        newCorrectFade[i] = 1;
                    }
                    else
                    {
                        newCorrectFade[i] = 0;
                    }
                }

                wordImages[i].material.SetFloat("_Amount", newCorrectFade[i]);*/

                // We first check if the meaning is correct, to either display the meaning as a text or the dropdown
                if (listKnownJournal[(currentPage*6) + i].correct)
                {
                    
                    // Disable the dropdown, enable the text
                    pageElement[i].transform.GetChild(1).gameObject.SetActive(false);
                    pageElement[i].transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = listKnownJournal[(currentPage * 6) + i].playerMeaning;
                }
                else
                {
                    // Disable the text, enable the dropdown
                    pageElement[i].transform.GetChild(1).gameObject.SetActive(true);

                    for (int j = 0; j < totalMeanings.Count; j++)
                    {
                        if (totalMeanings[j] == listKnownJournal[(currentPage * 6) + i].playerMeaning)
                        {
                            pageElement[i].transform.GetChild(1).GetComponent<TMP_Dropdown>().value = j;
                            pageElement[i].transform.GetChild(1).GetComponent<TMP_Dropdown>().RefreshShownValue();

                            // Flicker to update
                            pageElement[i].transform.GetChild(1).GetComponent<TMP_Dropdown>().enabled = false;
                            pageElement[i].transform.GetChild(1).GetComponent<TMP_Dropdown>().enabled = true;
                        }
                    }
                }
                

                // Lastly, update the "tick" mark if the word already has a correct association, and if so lock the dropdown.
                if (listKnownJournal[(currentPage * 6) + i].correct)
                {
                    pageElement[i].transform.GetChild(1).GetComponent<TMP_Dropdown>().interactable = false;
                    pageElement[i].transform.GetChild(2).gameObject.SetActive(true);
                }
                else
                {
                    pageElement[i].transform.GetChild(1).GetComponent<TMP_Dropdown>().interactable = true;
                    pageElement[i].transform.GetChild(2).gameObject.SetActive(false);
                }

            }
            else
            {
                pageElement[i].SetActive(false);
            }
        }

        // This will deal with all of the visual effects regarding validation (fading-in effect, getting notified about correct words on other pages)
        CheckNewValidation();

        // Draw notifications on left/right pages if there are new words there
        if(newCorrectBefore)
        {
            // Draw image above the left 
        }

        if(newCorrectAfter)
        {
            // Draw image above the right 
        }

        // Increase gradually the shader, until 1, for every newCorrect one (that should be set to 0.O1 at the start)

        for(int i = 0; i < newCorrectFade.Length; i++)
        {
            if ((newCorrectFade[i] > 0) && (newCorrectFade[i] < 1.0f))
            {
                newCorrectFade[i] += Time.deltaTime;
                Debug.Log(newCorrectFade[i] + " & i = " + i);
                if (newCorrectFade[i] > 1.0f)
                {
                    newCorrectFade[i] = 1.0f;
                }
            }
        }
    }

    public void CheckNewValidation()
    {
        newCorrectBefore = false;
        newCorrectAfter = false;

        // Check for newCorrect in the pages before 
        for(int i = 0; i < (currentPage * 6) - 1; i++)
        {
            if(listKnownJournal[i].newCorrect == true)
            {
                newCorrectBefore = true;
            }
        }

        // Check for newCorrect in the pages after
        for(int j = (currentPage * 6); j < listKnownJournal.Count; j++)
        {
            if (listKnownJournal[j].newCorrect == true)
            {
                newCorrectAfter = true;
            }
        }
    }

    public void SetRoundAmount()
    {
        StopAllCoroutines();
        for (int i = 0; i < pageElement.Length; i++)
        {
            if ((currentPage * 6) + i < listKnownJournal.Count)
            {
                    if (listKnownJournal[(currentPage * 6) + i].correct)
                    {
                        newCorrectFade[i] = 1;
                    }
                    else
                    {
                        newCorrectFade[i] = 0;
                    }

                wordImages[i].material.SetFloat("_Amount", newCorrectFade[i]);
                wordImages[i].material.SetFloat("_EdgeWidth", 0);
            }
        }
    }

    public void SaveData()
    {
        SaveSystem.SaveCarnet(this);
    }

    public void LoadData()
    {

        CarnetData cd = SaveSystem.LoadCarnet();
        
        if (cd != null)
        {
            /*for (int i = 0; i < listJournal.Count; i++)
            {
                if (correspondingBatch[i] < ols.currentBatch)
                {
                    listKnownJournal.Add(listJournal[i]);
                }
            }*/



            for (int i = 0; i < cd.removedWords.Length; i++)
            {
                for (int j = 0; j < totalMeanings.Count; j++)
                {
                    if (totalMeanings[j] == cd.removedWords[i])
                    {
                        removedWords.Add(totalMeanings[j]);
                        totalMeanings.RemoveAt(j);
                    }
                }
            }

            for (int l = 0; l < pageElement.Length; l++)
            {
                pageElement[l].transform.GetChild(1).GetComponent<TMP_Dropdown>().ClearOptions();
                pageElement[l].transform.GetChild(1).GetComponent<TMP_Dropdown>().AddOptions(totalMeanings);
            }

            AddWords();

            for (int i = 0; i < cd.currentWords.Length; i++)
            {
                listKnownJournal[i].playerMeaning = cd.currentWords[i];

                if (cd.validate[i])
                {
                    listKnownJournal[i].correct = true;
                }
            }

            UpdateCurrentPage();

            numberOfValidated = cd.numbervalidated;

            if (numberOfValidated >= unlockNextBatch[ols.currentBatch] && ols.currentBatch > 0)
            {
                FindObjectOfType<Telephone>().AddBatchNotif();
            }
        }
        else
        {
            for (int i = 0; i < listJournal.Count; i++)
            {
                if (correspondingBatch[i] == 0)
                {
                    listKnownJournal.Add(listJournal[i]);
                }
            }
        }


    }


    IEnumerator Dissolve(int id)
    {
        if ((currentPage * 6) + id < listKnownJournal.Count)
        {
            newCorrectFade[id] = 0;
            while (newCorrectFade[id] < 1)
            {
                newCorrectFade[id] += Time.deltaTime;
                wordImages[id].material.SetFloat("_EdgeWidth", newCorrectFade[id]);
                yield return 0;
            }

            newCorrectFade[id] = 0;
            while (newCorrectFade[id] < 1)
            {
                newCorrectFade[id] += Time.deltaTime;
                wordImages[id].material.SetFloat("_Amount", newCorrectFade[id]);
                yield return 0;
            }
        }  
    }
}

// We'll use this class to determine a dictionnary of all possible words and their associated meanings.
public class Word
{
    // Main components
    public Sprite word;            // The written word, as an image
    public string meaning;          // French equivalent of the written word

    // Contructor
    public Word(Sprite w, string m)
    {
        word = w;
        meaning = m;
    }

}

// We'll use this class for the journal, and to check for the words the player associated with each symbol
public class Pairing
{
    // Main components
    public int id;                  // This will be used to pair words when split up in the incomplete listJournalDiscovered
    public Word word;               // Which word we're talking about
    public string playerMeaning;    // What meaning the player is currently associating with the word

    public bool correct;            // Whether or nor the player has filled in the correct meaning for the word (and it was validated by batch validation)
    public bool newCorrect;         // Set to true once the player correctly guessed the meaning, but has not yet returned to its page to see it validated (triggers animation)

    // Constructor (this version works with only a word, it's meant for the initialization of listJournal)
    public Pairing(Word w)
    {
        id = 0;                     
        word = w;
        playerMeaning = "";

        correct = false;
        newCorrect = false;
    }

    public Pairing(int i, Word w)
    {
        id = i;
        word = w;
        playerMeaning = "";

        correct = false;
        newCorrect = false;
    }

}