﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;


public static class SaveSystem
{
    public static void SaveLiege(TableauLiege2 tl)
    {
        LiegeData data = new LiegeData(tl);



        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/liege.sav";
        FileStream stream = new FileStream(path, FileMode.Create);

        formatter.Serialize(stream, data);
        stream.Close();

    }

    public static void SavePositions(ObjectListState ols)
    {
        PositionsData data = new PositionsData(ols);
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/position.sav";
        FileStream stream = new FileStream(path, FileMode.Create);

        formatter.Serialize(stream, data);
        stream.Close();

        Debug.Log("file saved at " + path);
    }

    public static void SaveSetting(ScriptableOptions so)
    {
        SettingsData data = new SettingsData(so);



        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/options.sav";
        FileStream stream = new FileStream(path, FileMode.Create);

        formatter.Serialize(stream, data);
        stream.Close();

    }

    public static void SaveCarnet(DictionnaryControl cd)
    {
        CarnetData data = new CarnetData(cd);



        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/carnet.sav";
        FileStream stream = new FileStream(path, FileMode.Create);

        formatter.Serialize(stream, data);
        stream.Close();

    }

    public static LiegeData LoadLiege()
    {
        string path = Application.persistentDataPath + "/liege.sav";
        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);


            LiegeData data = formatter.Deserialize(stream) as LiegeData;
            stream.Close();
            return data;

        }
        else
        {
            Debug.Log("Not find Liege");
            return null;
        }
    }

    public static PositionsData LoadPositions()
    {
        string path = Application.persistentDataPath + "/position.sav";
        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);


            PositionsData data = formatter.Deserialize(stream) as PositionsData;
            stream.Close();
            return data;

        }
        else
        {
            Debug.Log("Not find pos");
            return null;
        }
    }

    public static SettingsData LoadSetting()
    {
        string path = Application.persistentDataPath + "/options.sav";
        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);


            SettingsData data = formatter.Deserialize(stream) as SettingsData;
            stream.Close();
            return data;

        }
        else
        {
            Debug.Log("Not find settings");
            return null;
        }
    }

    public static CarnetData LoadCarnet()
    {
        string path = Application.persistentDataPath + "/carnet.sav";
        if (File.Exists(path))
        {
            Debug.Log(path);
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);


            CarnetData data = formatter.Deserialize(stream) as CarnetData;
            stream.Close();
            return data;

        }
        else
        {
            Debug.Log("Not find carnet");
            return null;
        }
    }

    public static void RemoveSaves()
    {
        string path = Application.persistentDataPath + "/carnet.sav";
        if (File.Exists(path))
        {
            File.Delete(path);
        }

        path = Application.persistentDataPath + "/liege.sav";
        if (File.Exists(path))
        {
            File.Delete(path);
        }

        path = Application.persistentDataPath + "/options.sav";
        if (File.Exists(path))
        {
            File.Delete(path);
        }

        path = Application.persistentDataPath + "/position.sav";
        if (File.Exists(path))
        {
            File.Delete(path);
        }
    }
}

[System.Serializable]
public class LiegeData
{
    public float[] positionsX;
    public float[] positionsY;
    public float[] positionsZ;
    public int[] objectsID;

    public int[] lineColors;
    public int[] lineStarts;
    public int[] lineEnds;

    public LiegeData(TableauLiege2 tl)
    {
        positionsX = new float[tl.pined.Count];
        positionsY = new float[tl.pined.Count];
        positionsZ = new float[tl.pined.Count];
        objectsID = new int[tl.pined.Count];

        lineColors = new int[tl.lineStartsID.Count];
        lineStarts = new int[tl.lineStartsID.Count];
        lineEnds = new int[tl.lineStartsID.Count];

        for (int i = 0; i < tl.pined.Count; i++)
        {
            positionsX[i] = tl.pined[i].transform.position.x;
            positionsY[i] = tl.pined[i].transform.position.y;
            positionsZ[i] = tl.pined[i].transform.position.z;
            objectsID[i] = tl.pinedId[i];
        }

        for (int i = 0; i < tl.lineStartsID.Count; i++)
        {
            lineColors[i] = tl.linesColor[i];
            lineStarts[i] = tl.lineStartsID[i];
            lineEnds[i] = tl.lineEndsID[i];
        }
    }
}

[System.Serializable]
public class PositionsData
{
    public float[] posX;
    public float[] posY;
    public float[] posZ;
    public bool[] unlocked;
    public bool[] shelfed;
    public int currentBatch;

    public PositionsData(ObjectListState ols)
    {
        posX = new float[ols.positions.Length];
        posY = new float[ols.positions.Length];
        posZ = new float[ols.positions.Length];
        unlocked = new bool[ols.unlocked.Length];
        shelfed = new bool[ols.unlocked.Length];

        for (int i = 0; i < ols.positions.Length; i++)
        {
            posX[i] = ols.positions[i].x;
            posY[i] = ols.positions[i].y;
            posZ[i] = ols.positions[i].z;
        }

        for (int i = 0; i < ols.unlocked.Length; i++)
        {
            unlocked[i] = ols.unlocked[i];
            shelfed[i] = ols.shelfed[i];
        }

        currentBatch = ols.currentBatch;
    }
}

[System.Serializable]
public class SettingsData
{
    public float masterLevel;
    public float soundLevel;
    public float musiclevel;
    public float brightness;
    public int resolution;
    public int currentLanguage;
    public bool transcriptOn;
    public bool autoTranslate;

    public SettingsData(ScriptableOptions op)
    {
        masterLevel = op.masterLevel;
        soundLevel = op.soundLevel;
        musiclevel = op.musiclevel;
        brightness = op.brightness;
        resolution = op.currentResolution;
        currentLanguage = op.currentLanguage;
        transcriptOn = op.transcriptOn;
        autoTranslate = op.autoTranslate;
    }
}

[System.Serializable]
public class CarnetData
{
    public bool[] validate;
    public string[] removedWords;
    public string[] currentWords;
    public int numbervalidated;

    public CarnetData(DictionnaryControl dc)
    {
        validate = new bool[dc.listKnownJournal.Count];
        removedWords = new string[dc.removedWords.Count];
        currentWords = new string[dc.listKnownJournal.Count];
        numbervalidated = dc.numberOfValidated;

        for (int i = 0; i < dc.listKnownJournal.Count; i++)
        {
            currentWords[i] = dc.listKnownJournal[i].playerMeaning;
            validate[i] = dc.listKnownJournal[i].correct;
        }

        for (int i = 0; i < dc.removedWords.Count; i++)
        {
            removedWords[i] = dc.removedWords[i];
        }

    }
}
