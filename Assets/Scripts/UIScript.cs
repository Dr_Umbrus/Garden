﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;

public class UIScript : MonoBehaviour
{
    ObjectListState ols;
    ObjectInspection oi;
    ArchProgress ap;
    GameManager gm;
    ScriptableOptions so;

    [Header("Folder")]
    public bool folderActive = false;
    public GameObject folder;
    public Image folderPhoto;
    public TextMeshProUGUI description;
    public float folderSpeedLeft;
    public float folderSpeedRight;
    public float folderPosLeft;
    public float folderPosRight;
    public float folderLeftDiv= 5.75f;
    public float folderRightDiv= 4.25f;
    public LeanTweenType folderEnter;
    public LeanTweenType folderBack;
    public int currentFolderState;
    public GameObject lock1, lock2;
    public ScriptableColor[] colors;
    public Image Folderbg;
    [FMODUnity.EventRef]
    public string[] folderSound;

    [Header("Phone")]
    public GameObject phone;
    bool phoneState = false;
    public GameObject phoneLightUp;
    public GameObject phoneBar;
    Telephone phoneScript;
    public float phoneSpeedUp;
    public float phoneSpeedDown;
    public float phonePosUp;
    public float phonePosDown;
    public LeanTweenType phoneEnter;
    public LeanTweenType phoneBack;

    [Header("Carnet")]
    public GameObject carnetImage;
    public GameObject carnetCanvas;
    public float carnetSpeedGoingUp;
    public float carnetSpeedGoingDown;
    public float carnetPosUp;
    public float carnetPosDown;
    public float carnetUpDiv;
    public float carnetDownDiv;
    public LeanTweenType carnetGoingUp;
    public LeanTweenType carnetGoingDown;
    bool carnetActive=false;

    [Header("Misc")]
    public GameObject tuto;
    public int tutoState = -1;

    [Header("Reso")]
    public Text resoText;

    [Header("BoutonGros")]
    public GameObject boutonGrosR;
    public GameObject boutonGrosL;
    bool isGros = false;
    int maxStep = 0;

    private void Awake()
    {
        gm = FindObjectOfType<GameManager>();
        gm.uiS = this;
        phoneScript = phone.GetComponent<Telephone>();
        oi = FindObjectOfType<ObjectInspection>();
        ols = gm.ols;
        ap = gm.ap;
        so = gm.so;
        maxStep = FindObjectOfType<Tutoriel>().maxStep;
    }

    public void StartGame()
    {
        Folderbg.color = colors[0].col;
    }


    #region folder
    public void ChangeFolderState()
    {
        if (!folderActive)
        {
            ActivateFolder();
            folderActive = true;
        }
        else
        {
            folderActive = false;
            StopFolder();
        }
    }

    public void ActivateFolder()
    {
        if (tutoState < maxStep)
        {
            Tutoriel tutoText = FindObjectOfType<Tutoriel>();
            if (tutoText != null)
            {
                tutoText.UpdateDossier();
                tutoState = tutoText.tutoStep;
            }
            else
            {
                tutoState = maxStep;
            }
        }
        tuto.SetActive(false);
        if (gm.so.resolutions.Length > 0)
        {
            folderPosRight = (gm.so.resolutions[gm.so.currentResolution].width) / folderRightDiv;
        }
        else
        {
            folderPosRight = 1920 / folderRightDiv;
        }

        ChangeFolderDescription();
        FMODUnity.RuntimeManager.PlayOneShotAttached(folderSound[0], gameObject);
        LeanTween.moveX(folder, folderPosRight, folderSpeedRight).setEase(folderEnter);

    }

    public void StopFolder()
    {
        if (tutoState < maxStep)
        {
            tuto.SetActive(true);
        }

        if (gm.so.resolutions.Length > 0)
        {
            folderPosLeft = -(gm.so.resolutions[gm.so.currentResolution].width) / folderLeftDiv;
        }
        else
        {
            folderPosLeft = -1920 / folderLeftDiv;
        }
        FMODUnity.RuntimeManager.PlayOneShotAttached(folderSound[1], gameObject);
        LeanTween.moveX(folder, folderPosLeft, folderSpeedLeft).setEase(folderBack);
    }

    public void ChangeFolderDescription()
    {
        Folderbg.color = colors[currentFolderState].col;

        int currentObjectType = -1;

        if (oi.movingObject != null)
        {
            if (oi.movingObject.tag != "BigObject")
            {
                currentObjectType = 0;
                for (int i = 0; i < ols.descriptions.Length; i++)
                {
                    if (oi.movingObject.name.Contains(ols.allObjects[i].name))
                    { 
                        folderPhoto.sprite = ols.photos[i];
                    }
                }
            }
            else
            {
                currentObjectType = 1;
                for (int i = 0; i < ols.grosDescription.Length; i++)
                {
                    if (oi.movingObject.name.Contains(ols.grosObjects[i].name))
                    {
                        folderPhoto.sprite = ols.grosPhotos[i];
                    }
                }
            }
        }

        //affiche la description du batch actuelle quand aucun objet n est examiné
        else
        {
            currentObjectType = 2;
            folderPhoto.sprite = ols.batchPhotos;
        }

        if (currentFolderState == 0)
        {
            if (currentObjectType == 0)
            {
                for (int i = 0; i < ols.descriptions.Length; i++)
                {
                    if (oi.movingObject.name.Contains(ols.allObjects[i].name))
                    {
                        description.SetText(ols.descriptions[i]);
                    }
                }
            }
            else if(currentObjectType == 1)
            {
                for (int i = 0; i < ols.grosDescription.Length; i++)
                {
                    if (oi.movingObject.name.Contains(ols.grosObjects[i].name))
                    {
                        description.SetText(ols.grosDescription[i]);
                    }
                }
            }
            else
            {
                description.SetText(ols.batchDescriptions[ols.currentBatch]);
            }
        }
        else if (currentFolderState == 1)
        {

            description.SetText(ols.firstHint[ols.currentBatch]);
        }
        else
        {
            description.SetText(ols.secondHint[ols.currentBatch]);
        }
    }

    public void ChangeOnglet(int onglet)
    {
        if((onglet==1 && lock1.activeSelf) || (onglet == 2 && lock2.activeSelf))
        {

        }
        else
        {
            currentFolderState = onglet;
            ChangeFolderDescription();
            if (!folderActive)
            {
                ActivateFolder();
            }
        }
    }

    public void NewBatch()
    {
        currentFolderState = 0;
        ChangeFolderDescription();
        if (folderActive)
        {
            ChangeFolderState();
        }
        LockBack();
    }

    public void RemoveLock(bool first)
    {
        if (first)
        {
            lock1.SetActive(false);
        }
        else
        {
            lock2.SetActive(false);
        }
    }

    public void LockBack()
    {
        lock1.SetActive(true);
        lock2.SetActive(true);
    }

    #endregion

    #region carnet
    public void ActivateCarnet()
    {
        if (carnetActive)
        {
            CarnetDown();
        }
        else
        {
            CarnetUp();
        }
        carnetActive = !carnetActive;
    }

    public void CarnetUp()
    {
        carnetCanvas.SetActive(true);
        if (gm.so.resolutions.Length > 0)
        {
            carnetPosUp = -(gm.so.resolutions[gm.so.currentResolution].height) / carnetUpDiv;
        }
        else
        {
            carnetPosUp = -1080 / carnetUpDiv;
        }

        //FMODUnity.RuntimeManager.PlayOneShotAttached(folderSound[0], gameObject);
        LeanTween.moveLocalY(carnetImage, carnetPosUp, carnetSpeedGoingUp).setEase(carnetGoingUp);
    }

    public void CarnetDown()
    {
        if (gm.so.resolutions.Length > 0)
        {
            carnetPosDown = -(gm.so.resolutions[gm.so.currentResolution].height) / carnetDownDiv;
        }
        else
        {
            carnetPosDown = -1080 / carnetDownDiv;
        }

        //FMODUnity.RuntimeManager.PlayOneShotAttached(folderSound[0], gameObject);
        LeanTween.moveLocalY(carnetImage, carnetPosDown, carnetSpeedGoingDown).setEase(carnetGoingDown);
        StartCoroutine(WaitCarnetDisparition());
    }

    IEnumerator WaitCarnetDisparition()
    {
        yield return new WaitForSeconds(carnetSpeedGoingDown);
        carnetCanvas.SetActive(false);
    }

    #endregion

    #region phone
    public void OnPhone()
    {
        LightUpPhone();
        LeanTween.moveY(phone, phonePosUp, phoneSpeedUp).setEase(phoneEnter);
    }

    public void LightUpPhone()
    {
        phoneLightUp.SetActive(true);
        phoneBar.SetActive(true);
    }

    public void LightDownPhone()
    {
        phoneLightUp.SetActive(false);
        phoneBar.SetActive(false);
    }

    public void BackPhone()
    {
        LightDownPhone();
        LeanTween.moveY(phone, phonePosDown, phoneSpeedDown).setEase(phoneBack);
    }

    public void Vibrate()
    {

    }
    #endregion

   

    #region resolution
    public void ChangeResolution(bool plus)
    {
        if (plus)
        {
            if (so.currentResolution < so.resolutions.Length - 1)
            {
                so.currentResolution++;
            }
            else
            {
                so.currentResolution = 0;
            }
        }
        else
        {
            if (so.currentResolution > 0)
            {
                so.currentResolution--;
            }
            else
            {
                so.currentResolution = so.resolutions.Length - 1;
            }
        }

        resoText.text = so.resolutions[so.currentResolution].width + "x" + so.resolutions[so.currentResolution].height;
    }

    public void ValidateResolution()
    {
       gm.ChangeResolution(so.currentResolution);
    }
    #endregion

    #region vue gros objets
    public void SetVueGrosObjet()
    {
        isGros = !isGros;
        if (isGros)
        {
            boutonGrosL.SetActive(true);
            boutonGrosR.SetActive(false);
        }
        else
        {
            boutonGrosL.SetActive(false);
            boutonGrosR.SetActive(true);
        }
    }
    #endregion
}
