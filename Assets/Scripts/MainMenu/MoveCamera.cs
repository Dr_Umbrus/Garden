﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCamera : MonoBehaviour
{
    public GameManager quitFonction;
    public MovePorte porte;
    public DisplayMenu MenuDisplay;
    public GameObject Camera;
    public GameObject[] CameraPos;
    public bool start = true;
    public bool end = false;
    // Start is called before the first frame update

    public void Start()
    {
        if (start)
        {
            Camera.transform.position = CameraPos[4].transform.position;
            Camera.transform.rotation = CameraPos[4].transform.rotation;
        }
    }
    public void VueBureau()
    {
        StartCoroutine(Move(Camera.transform, CameraPos[0].transform.position, Quaternion.Euler(CameraPos[0].transform.localEulerAngles), 0.5f));
    }

    public void VueTiroir()
    {
        StartCoroutine(Move(Camera.transform, CameraPos[1].transform.position, Quaternion.Euler(CameraPos[1].transform.localEulerAngles), 0.7f));
    }

    public void VueJouer()
    {
        StartCoroutine(Move(Camera.transform, CameraPos[2].transform.position, Quaternion.Euler(CameraPos[2].transform.localEulerAngles),0.5f));
    }

    public void VuePorte()
    {
        StartCoroutine(Move(Camera.transform, CameraPos[3].transform.position, Quaternion.Euler(CameraPos[3].transform.localEulerAngles), 1f));
    }

    public void VueQuitter()
    {
        StartCoroutine(Move(Camera.transform, CameraPos[4].transform.position, Quaternion.Euler(Camera.transform.localEulerAngles), 2f));
        end = true;
    }

    public void VueTransi()
    {
        StartCoroutine(Move(Camera.transform, CameraPos[5].transform.position, Quaternion.Euler(CameraPos[5].transform.localEulerAngles), 2f));
    }


    public IEnumerator Move(Transform objectInspect, Vector3 position, Quaternion newRotation, float timeToMove)
    {
        var currentPos = objectInspect.position;
        var t = 0f;
        //while pour le déplacement et rotation smooth
        while (t < 1)
        {
            t += Time.deltaTime / timeToMove;
            objectInspect.position = Vector3.Lerp(currentPos, position, t);
            objectInspect.rotation = Quaternion.Lerp(objectInspect.transform.rotation, newRotation, t);
            yield return null;
        }
        if (start)
        {
            StartCoroutine(Move(Camera.transform, CameraPos[0].transform.position, Quaternion.Euler(CameraPos[0].transform.localEulerAngles), 2f));
            porte.Close();
            MenuDisplay.displayPrincipal();
            start = false;
        }
        if (end)
        {
            quitFonction.Quit();
        }
    }
}
