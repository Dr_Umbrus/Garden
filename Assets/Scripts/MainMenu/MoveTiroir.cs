﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTiroir : MonoBehaviour
{
    public GameObject[] tiroirPos;

    [FMODUnity.EventRef]
    public string tiroirMove;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    public void Open()
    {
        StartCoroutine(Move(transform, tiroirPos[1].transform.position, 1f, .5f));
    }

    public void Close()
    {
        StartCoroutine(Move(transform, tiroirPos[0].transform.position, 1f, 0));
    }


    public IEnumerator Move(Transform objectInspect, Vector3 position, float timeToMove, float wait)
    {
        yield return new WaitForSeconds(wait);
        FMODUnity.RuntimeManager.PlayOneShotAttached(tiroirMove, gameObject);
        var currentPos = objectInspect.position;
        var t = 0f;
        //while pour le déplacement et rotation smooth
        while (t < 1)
        {
            t += Time.deltaTime / timeToMove;
            objectInspect.position = Vector3.Lerp(currentPos, position, t);
            yield return null;
        }
    }
}
