﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePorte : MonoBehaviour
{
    public Vector3[] posPorte;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    public void Open()
    {
        StartCoroutine(Move(transform, Quaternion.Euler(posPorte[1]), 1f));
        Debug.Log("OUVERT");
    }
    public void Open2()
    {
        StartCoroutine(Move(transform, Quaternion.Euler(posPorte[2]), 1f));
        Debug.Log("Sortie");
    }

    public void Close()
    {
        StartCoroutine(Move(transform, Quaternion.Euler(posPorte[0]), 1f));
        Debug.Log("Fermé");
    }

    public IEnumerator Move(Transform objectInspect, Quaternion newRotation, float timeToMove)
    {
        var t = 0f;
        //while pour le déplacement et rotation smooth
        while (t < 1)
        {
            t += Time.deltaTime / timeToMove;
            objectInspect.rotation = Quaternion.Lerp(objectInspect.transform.rotation, newRotation, t);
            yield return null;
        }
    }
}
