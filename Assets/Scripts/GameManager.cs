﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using FMODUnity;
using FMOD.Studio;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [Header("OtherScripts")]
    TableauLiege2 tl = null;
    public ArchProgress ap = null;
    public DictionnaryControl dc = null;
    public UIScript uiS;
    public ObjectInspection oi;
    public Telephone tel;


    [Header("Scriptable")]
    public ObjectListState ols;
    public ScriptableOptions so;

    [Header("SoundProperties")]
    Bus masterBus;
    public string masterBusPath;
    Bus musicBus;
    public string musicBusPath = "Music";
    Bus sdBus;
    public string sdBusPath = "SoundEffect";

    string mainScene = "MainScene";

    [Header("GameThings")]
    public Image darkness;

    [Header("MainMenu")]
    public bool isInGame = false;
    public GameObject activateInGame;
    public GameObject mainMenu;

    [Header("MainMusic")]
    [EventRef]
    public string mainMusic;
    public EventInstance soundevent;

    [Header("Misc")]
    public bool bigView = false;
    public int currentItems = 0;
    public Tutoriel tuto;


    private static GameManager _instance;

    public static GameManager Instance { get { return _instance; } }

    private void Awake()
    {
        soundevent = RuntimeManager.CreateInstance(mainMusic);
        RuntimeManager.AttachInstanceToGameObject(soundevent, GetComponent<Transform>(), GetComponent<Rigidbody>());
        soundevent.start();

        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        DontDestroyOnLoad(gameObject);

        LoadOptions();
        activateInGame.SetActive(false);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            RemoveSaves();
        }

        if (isInGame)
        {
            ap.UpdateMe();
            oi.UpdateMe();
            dc.UpdateMe();
            //ro.UpdateMe();
        }

        RuntimeManager.StudioSystem.setParameterByName("Hour", System.DateTime.Now.Hour + (System.DateTime.Now.Minute / 60f));
    }

    public void StartGame(bool newGame)
    {
        mainMenu.SetActive(false);
        if (newGame)
        {
            RemoveSaves();
        }
        if (ols.currentBatch > 0)
        {
            EndTuto();
        }
        FindObjectOfType<PickPhone>().EnablePick();
        activateInGame.SetActive(true);
        ap.GameStart();
        dc.GameStart();
        uiS.StartGame();
        isInGame = true;
    }

    public void OpenJournal()
    {
        uiS.ActivateCarnet();
        dc.OpenJournal();
    }

    public void EndTuto()
    {
        dc.tutoState = 12;
        uiS.tutoState = 12;
        
        if (tuto != null)
        {
            Debug.Log("ok");
            tuto.tutoStep = 12;
            tuto.gameObject.SetActive(false);
        }



    }


    public void Save()
    {

        if (isInGame)
        {
            if (tl != null)
            {
                SaveSystem.SaveLiege(tl);
            }

            if (ols != null && ap != null)
            {
                for (int i = 0; i < ols.positions.Length; i++)
                {
                    if (ols.unlocked[i] && !ols.shelfed[i])
                    {
                        ols.positions[i] = ap.allObjectsPos[i].position;
                    }
                }

                SaveSystem.SavePositions(ols);
            }

            if (dc != null)
            {
                SaveSystem.SaveCarnet(dc);
            }
            else
            {
                Debug.Log("nocar");
            }

        }


        if (so != null)
        {
            SaveSystem.SaveSetting(so);
        }
        else
        {
            Debug.Log("NoOptions");
        }

    }


    public void ChangeLights(float newBright)
    {
        so.brightness = newBright;
        darkness.color = Color.Lerp(so.darkest.col, so.brightest.col, so.brightness);
    }

    public void ChangeMasterVol(float newLevel)
    {
        masterBus.setVolume(newLevel);
        so.masterLevel = newLevel;
    }

    public void ChangeMusicVol(float newLevel)
    {
        musicBus.setVolume(newLevel);
        so.musiclevel = newLevel;
    }

    public void ChangeSoundVol(float newLevel)
    {
        sdBus.setVolume(newLevel);
        so.soundLevel = newLevel;
    }

    public void ChangeResolution(int newRes)
    {
        if (so.resolutions.Length == 0)
        {
            Screen.SetResolution(1920, 1080, false, 60);
        }
        else
        {
            so.currentResolution = newRes;
            Screen.SetResolution(so.resolutions[so.currentResolution].width, so.resolutions[so.currentResolution].height, false, so.resolutions[so.currentResolution].refreshRate);
        }

    }

    void LoadOptions()
    {
        so.CheckRes();
        masterBus = RuntimeManager.GetBus("bus:/Master");
        musicBus = RuntimeManager.GetBus("bus:/Master/" + musicBusPath);
        sdBus = RuntimeManager.GetBus("bus:/Master/" + sdBusPath);
        so.LoadMe(SaveSystem.LoadSetting());
        ChangeLights(so.brightness);
        ChangeMasterVol(so.masterLevel);
        ChangeMusicVol(so.musiclevel);
        ChangeSoundVol(so.soundLevel);
        ChangeResolution(so.currentResolution);
    }


    public void countObjects()
    {
        Debug.Log("jsp");
        //utilisé la liste dans archprogress
        currentItems = ap.GetActiveObjects();

        if (currentItems > 5)
        {
            if (tel.state == Telephone.PhoneStates.Sleep)
            {
                tel.gameObject.GetComponent<PickPhone>().TakeLeave();
            }
            tel.ChangeState(Telephone.PhoneStates.Storage);
            //Warning.SetActive(true);
        }
        else
        {
            //Warning.SetActive(false);
        }

    }

    #region Camera
    //fonction pour changer la vue de la caméra entre bureau et rangement
    public void SwitchViewGrosObjets()
    {
        if (!oi.inspectiON)
        {
            uiS.SetVueGrosObjet();
            if (!bigView)
            {
                bigView = !bigView;
                StartCoroutine(MoveToStore(GameObject.Find("Main Camera").transform, GameObject.Find("GrosObjectView").transform.position, GameObject.Find("GrosObjectView").transform.rotation, 0.5f));
            }
            //changement vue bureau
            else
            {
                bigView = !bigView;
                StartCoroutine(MoveToStore(GameObject.Find("Main Camera").transform, GameObject.Find("CameraView").transform.position, GameObject.Find("CameraView").transform.rotation, 0.5f));
            }
        }
    }



    //coroutine pour bouger les objets vers le rangement
    public IEnumerator MoveToStore(Transform objectInspect, Vector3 position, Quaternion newRotation, float timeToMove)
    {
        var currentPos = objectInspect.position;
        var t = 0f;
        //while pour le déplacement et rotation smooth
        while (t < 1)
        {
            t += Time.deltaTime / timeToMove;
            objectInspect.position = Vector3.Lerp(currentPos, position, t);
            objectInspect.rotation = Quaternion.Lerp(objectInspect.transform.rotation, newRotation, t);
            yield return null;
        }

    }
    #endregion


    public void ChangeScene(string newSceneName)
    {
        SceneManager.LoadScene(newSceneName);

    }

    public void RemoveSaves()
    {
        SaveSystem.RemoveSaves();
    }

    public void Quit()
    {
        Application.Quit();
    }

    private void OnApplicationQuit()
    {
        Save();
    }

    public void StepMenu(float step)
    {
        RuntimeManager.StudioSystem.setParameterByName("StepInMenu", step);
    }

    public void Transition(float enigm, bool play)
    {
        Debug.Log("Weshalors ?!!!");
        StartCoroutine(ControlMusic(enigm, play));
        RuntimeManager.StudioSystem.setParameterByName("Enigme", enigm);
    }

    public IEnumerator ControlMusic(float enigm, bool play)
    {
        if (play)
        {
            Debug.Log("je vais lancer la musique lol");
            RuntimeManager.StudioSystem.setParameterByName("Let'sPlay", 0);
            soundevent.start();
        }
        else
        {
            Debug.Log("je vais stopper la musique lol");
            RuntimeManager.StudioSystem.setParameterByName("Let'sPlay", 1);
            yield return new WaitForSeconds(10f);
            soundevent.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
        }
    }
}
