﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Tutoriel : MonoBehaviour
{
    GameManager gm;
    public Telephone phoneScript;
    public ObjectInspection inspectionScript;
    public ArchProgress batchState;
    public DictionnaryControl journalScript;
    public GameObject textTuto;
    public GameObject canvasTuto;
    public GameObject HightLight;

    public int tutoStep = 0;
    public int maxStep = 12;
    // Start is called before the first frame update
    void Start()
    {
        gm = FindObjectOfType<GameManager>();
    }

    public void EndTuto()
    {
        gm.EndTuto();
    }

    // Update is called once per frame
    void Update()
    {
        if(inspectionScript.inspectiON && tutoStep == 0)
        {
            tutoStep++;
            textTuto.GetComponent<TextMeshProUGUI>().text = "Hé ben, il avait pas mal de choses à dire (souris et boutons pour faire tourner)";
        }
        else if (inspectionScript.movingObject==null && tutoStep == 1)
        {
            tutoStep++;
            textTuto.GetComponent<TextMeshProUGUI>().text = "J'ai reçu une notification sur mon téléphone, ça doit être les premiers arrivages";
        }
        else if(phoneScript.state == Telephone.PhoneStates.MainMenu && tutoStep == 2)
        {
            tutoStep++;
            textTuto.GetComponent<TextMeshProUGUI>().text = "Il est temps de se mettre au travail, on dirait (icone messages)";
        }
        else if (batchState.currentlyBatch && tutoStep == 3)
        {
            tutoStep++;
            textTuto.GetComponent<TextMeshProUGUI>().text = "Je devrais jeter oeil à chacun de ces objets, avant tout";
        }
        else if (!batchState.currentlyBatch && tutoStep == 4)
        {
            tutoStep++;
            textTuto.GetComponent<TextMeshProUGUI>().text = "Il est temps d'essayer cette appli qu'il a mentionné (téléphone)";
        }
        else if(phoneScript.state == Telephone.PhoneStates.MainMenu && tutoStep == 5)
        {
            tutoStep++;
            textTuto.GetComponent<TextMeshProUGUI>().text = "Ca doit être cette icône en forme de jarre, je suppose";
        }
        else if(phoneScript.state == Telephone.PhoneStates.Storage && tutoStep == 6)
        {
            tutoStep++;
            textTuto.GetComponent<TextMeshProUGUI>().text = "Essayons de sélectionner des objets à envoyer";
        }
        else if(batchState.activeObjects.Count < 5 && tutoStep == 7)
        {
            tutoStep++;
            textTuto.GetComponent<TextMeshProUGUI>().text = "J'ai aussi accès à quelques infos depuis l'appli, on dirait (clic droit sur les objets)";
        }
        else if(phoneScript.currentlySelected != -1 && tutoStep == 8)
        {
            tutoStep++;
            textTuto.GetComponent<TextMeshProUGUI>().text = "Je devrais inspecter cette carte qu'il a mentionné (bouton Gros objets)";
            HightLight.SetActive(true);
        }
        else if (gm.bigView && tutoStep == 9)
        {
            tutoStep++;
            textTuto.GetComponent<TextMeshProUGUI>().text = "Il m'a laissé des dossier pour chaque arrivages et objets, ça devrait m'être utile (clic dossier à gauche de l'écran)";
            HightLight.SetActive(false);
        }
    }

    public void UpdateCarnet()
    {
        if (journalScript.journalCanvas.activeSelf && tutoStep == 11)
        {
            tutoStep++;
            textTuto.GetComponent<TextMeshProUGUI>().text = " ";
            gm.EndTuto();
        }
    }

    public void UpdateDossier()
    {
        if (tutoStep == 10)
        {
            tutoStep++;
            textTuto.GetComponent<TextMeshProUGUI>().text = "Bon commençons ce travail de traduction, voyons ce que j'ai pour l'instant (bouton carnet)";
        }

    }
}
