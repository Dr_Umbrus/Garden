﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickPhone : MonoBehaviour
{
    ObjectInspection oi;
    public Telephone telScript;
    public ObjectInspection inspectionScript;
    public DictionnaryControl journalScript;
    public GameObject phoneTakenPos;
    public GameObject phoneNTPos;
    private bool isMoving = false;
    public bool picked = false;
    public bool CanPick = false;
    [FMODUnity.EventRef]
    public string zoomIn, zoomOut;

    void Awake()
    {
        oi = FindObjectOfType<ObjectInspection>();
        telScript = gameObject.GetComponent<Telephone>();
        inspectionScript = GameObject.Find("GameManager").GetComponent<ObjectInspection>();
        journalScript = GameObject.Find("GameManager").GetComponent<DictionnaryControl>();
    }


    private void OnMouseDown()
    {
        if (CanPick)
        {
            if (telScript.state == Telephone.PhoneStates.Sleep)
            {
                TakeLeave();
            }
        }   
    }

    //allume/éteint le tél
    public void OnOff()
    {
        if (CanPick)
        {
            if (telScript.state == Telephone.PhoneStates.Sleep)
            {
                if (inspectionScript.inspectiON)
                {
                    inspectionScript.endInspection();
                }
                if (journalScript.journalCanvas.activeSelf)
                {
                    journalScript.OpenJournal();
                }
                telScript.ChangeState(Telephone.PhoneStates.MainMenu);
                return;
            }

            if (telScript.state != Telephone.PhoneStates.Sleep)
            {
                Debug.Log("Ok");
                telScript.ChangeState(Telephone.PhoneStates.Sleep);
                return;
            }
        }        
    }

    //prend ou pose le tél
    public void TakeLeave()
    {
        if (CanPick)
        {
            if (!oi.inspectiON && !isMoving)
            {
                if (telScript.state == Telephone.PhoneStates.Sleep)
                {
                    OnOff();
                    StartCoroutine(MovePhone(this.transform, phoneTakenPos.transform.position, phoneTakenPos.transform.rotation, 0.5f, false));
                    FMODUnity.RuntimeManager.PlayOneShotAttached(zoomIn, gameObject);
                    picked = true;
                    return;
                }

                else
                {
                    OnOff();
                    StartCoroutine(MovePhone(this.transform, phoneNTPos.transform.position, phoneNTPos.transform.rotation, 0.5f, true));
                    FMODUnity.RuntimeManager.PlayOneShotAttached(zoomOut, gameObject);
                    telScript.gm.Save();
                    picked = false;

                    return;
                }
            }
        }  
    }

    public void EnablePick()
    {
        CanPick = true;
    }



    public IEnumerator MovePhone(Transform phone, Vector3 position, Quaternion newRotation, float timeToMove, bool turningOff)
    {
        if (!isMoving)
        {
            isMoving = true;
            var currentPos = phone.position;
            var t = 0f;
            //while pour le déplacement et rotation smooth
            while (t < 1)
            {
                t += Time.deltaTime / timeToMove;
                phone.position = Vector3.Lerp(currentPos, position, t);
                phone.rotation = Quaternion.Lerp(phone.transform.rotation, newRotation, t);
                yield return null;
            }
            isMoving = false;
        }
    }

}
